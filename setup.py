from setuptools import setup

with open("README.md", "r", encoding="utf-8") as f:
    long_description = f.read()

with open("requirements.txt", "r") as f:
    requirements = f.read()
requirements = [x for x in requirements.split("\n") if x != ""]


setup(
    name="tnqcircuits",
    version="0.0.1",
    description=("Tensor Network based Quantum Circuits"),
    long_description=long_description,
    long_description_content_type="text/markdown",
    url="https://gitlab.com/jackaraz/tnqcircuits",
    project_urls={
        "Bug Tracker": "https://gitlab.com/jackaraz/tnqcircuits/-/issues",
    },
    author="J.Y. Araz",
    author_email=("jack.araz@durham.ac.uk"),
    license="MIT",
    package_dir={"": "src"},
    install_requires=requirements,
    python_requires=">=3.6",
    classifiers=[
        "Intended Audience :: Science/Research",
        "License :: OSI Approved :: MIT License",
        "Operating System :: OS Independent",
        "Programming Language :: Python :: 3",
        "Topic :: Scientific/Engineering :: Artificial Intelligence",
        "Topic :: Scientific/Engineering :: Physics",
    ],
)
