#!/usr/bin/env python3
# -*- coding: utf-8 -*-

import argparse
import json
import os

os.environ['TF_CPP_MIN_LOG_LEVEL'] = '3'

def main(args):
    import tnqcircuits as tnq
    from tnqcircuits.generators.utils import preprocess
    import tensorflow as tf
    tf.keras.backend.set_floatx('float64')
    from tensorflow.keras.callbacks import (ReduceLROnPlateau, TerminateOnNaN, EarlyStopping,
                                            TensorBoard, ModelCheckpoint)

    #################
    # Preprocessing #
    #################

    train_dict = {
        "sig": [os.path.join(args.DATASETPATH, f"train/tt_CovNet_{x:04d}.npz") for x in
                range(1, min(args.NTRAINFILES + 1, 123))],
        "bkg": [os.path.join(args.DATASETPATH, f"train/QCD_CovNet_{x:04d}.npz") for x in
                range(1, min(args.NTRAINFILES + 1, 123))]
    }
    val_dict = {
        "sig": [os.path.join(args.DATASETPATH, f"val/tt_CovNet_{x:04d}.npz") for x in
                range(1, min(args.NVALFILES + 1, 42))],
        "bkg": [os.path.join(args.DATASETPATH, f"val/QCD_CovNet_{x:04d}.npz") for x in
                range(1, min(args.NVALFILES + 1, 42))]
    }

    processor = preprocess
    if args.NQUBITS == 36:
        processor = {
            "slice_length"      : 12,
            "training_data_path": os.path.join(args.DATASETPATH, "train"),
            "unravel_mode"      : "reshape" if args.ANSATZ == "ttn" else "eta-based",
            "return_downsampled": True,
        }
    elif args.NQUBITS == 16:
        processor = {
            "slice_length"      : 14,
            "training_data_path": os.path.join(args.DATASETPATH, "train"),
            "unravel_mode"      : "eta-based",
            "return_downsampled": True,
        }

    training_sample = tnq.generators.TNDataGenerator(
        smp_path_dict = train_dict, batch_size = args.NBATCH, nqubits = args.NQUBITS,
        shuffle = True, dtype = tf.float64, generatortype = "c", nevt_lim = args.NEVT,
        processor = processor, hilbert_dims = args.HILBERT,
        return_square = (args.NQUBITS == 36 and args.ANSATZ == "ttn"),
    )

    if args.NQUBITS in [36, 16]:
        processor.update({"scale": training_sample.processor.scale})

    validation_sample = tnq.generators.TNDataGenerator(
        smp_path_dict = val_dict, batch_size = args.NBATCH, nqubits = args.NQUBITS,
        shuffle = True, dtype = tf.float64, generatortype = "c", nevt_lim = args.NEVT,
        processor = processor, hilbert_dims = args.HILBERT,
        return_square = (args.NQUBITS == 36 and args.ANSATZ == "ttn"),
    )

    #################
    # Setup Ansatz  #
    #################

    if args.ANSATZ == "ttn":
        shape = [args.NQUBITS] if args.NQUBITS != 36 else [6, 6]
        model = tf.keras.Sequential(
            [tf.keras.layers.InputLayer((*shape, args.HILBERT,),dtype = tf.float64),
             tnq.layers.TTN(
                 n_nodes = args.NQUBITS, hilbert_dim = args.HILBERT, bond_dim = args.BOND,
                 output_dim = 2, execute_as = "model", activation = args.ACTIVATION,
             )], name = "TTN",
        )
    elif args.ANSATZ == "mps":
        model = tf.keras.Sequential(
            [tf.keras.layers.InputLayer((args.NQUBITS, args.HILBERT,), dtype = tf.float64),
             tnq.layers.MPS(
                 n_nodes = args.NQUBITS, hilbert_dim = args.HILBERT, bond_dim = args.BOND,
                 output_dim = 2, activation = args.ACTIVATION, pbc = args.PBC,
                 dtype = tf.float64,
             )], name = "MPS",
        )
    elif args.ANSATZ == "mera":
        assert args.NQUBITS != 36, "MERA is not defined for 36 nodes."
        model = tf.keras.Sequential(
            [tf.keras.layers.InputLayer((args.NQUBITS, args.HILBERT,), dtype = tf.float64),
             tnq.layers.MERA(
                 n_nodes = args.NQUBITS, hilbert_dim = args.HILBERT, bond_dim = args.BOND,
                 output_dim = 2, activation = args.ACTIVATION,
             )], name = "MERA",
        )

    ################
    # Train Ansatz #
    ################

    losses = {
        "mse"                     : tf.losses.MeanSquaredError(name = "mse"),
        "mae"                     : tf.losses.MeanAbsoluteError(name = "mae"),
        "categorical_crossentropy": tf.losses.CategoricalCrossentropy(name = "xentropy")
    }

    model.compile(
        optimizer = args.OPTIMIZER, loss = losses[args.LOSS],
        metrics = [tf.metrics.CategoricalAccuracy(name = "acc"), tf.metrics.AUC(name = "auc"),
                   tf.metrics.KLDivergence(name = "kld"),
                   tf.metrics.Precision(name = "precision")]
    )
    model.optimizer.lr = args.LR
    model.summary()

    callbacks = [
        ModelCheckpoint(
            os.path.join(args.OUTPUTPATH, "weights.h5"), monitor = 'val_loss', verbose = 1,
            save_best_only = True
        ),
        EarlyStopping(
            verbose = 1, patience = args.EPOCHS // 2, monitor = 'val_loss', mode = 'min',
            min_delta = 0.01
        ),
        ReduceLROnPlateau(
            monitor = 'val_loss', factor = 0.5, patience = 25, verbose = 1,
            mode = 'min', min_delta = 0.01, cooldown = 0, min_lr = 1e-8
        ),
        TerminateOnNaN(),
        TensorBoard(
            log_dir = os.path.join(args.LOGPATH, args.OUTPUTPATH.split("/")[-1])
        )
    ]

    try:
        history = model.fit(
            x = training_sample, epochs = args.EPOCHS, validation_data = validation_sample,
            callbacks = callbacks, verbose = 1
        )
    except KeyboardInterrupt:
        print("   * Training stopped by the user.")

    print(f"\n   * Output path: {args.OUTPUTPATH}\n")


if __name__ == "__main__":

    parser = argparse.ArgumentParser(
        description = "Train classical Tensor Network-based Neural Network."
    )

    ansatz = parser.add_argument_group("Ansatz handling")
    ansatz.add_argument(
        "ANSATZ", type = str, choices = ["ttn", "mps", "mera"],
        help = "Tensor Network based Quantum Classifier ansatz."
    )
    ansatz.add_argument(
        "-nq", "--num-qubits", type = int, choices = [4, 6, 8, 16, 36], default = 4,
        dest = "NQUBITS", help = "Number of qubits, default 4"
    )
    ansatz.add_argument(
        "-d", "--bond-dim", type = int, default = 2, dest = "BOND",
        help = "Bond dimensions, default 2"
    )
    ansatz.add_argument(
        "-hdim", "--hilbert-dim", default = 2, dest = "HILBERT", type = int,
        help = "Hilbert space dimensions. Default 2"
    )
    ansatz.add_argument(
        "-pbc", action = "store_true", default = False, dest = "PBC",
        help = "Periodic boundary condition, default false."
    )

    hyperparamgroup = parser.add_argument_group("Hyperparameter handling")
    hyperparamgroup.add_argument(
        "-e", "--epochs", type = int, dest = "EPOCHS", help = "Number of epochs, default 100",
        default = 100
    )
    hyperparamgroup.add_argument(
        "-nb", "--nbatch", type = int, dest = "NBATCH",
        help = "Number of events in a single batch, default 50", default = 50
    )
    hyperparamgroup.add_argument(
        "-nevt", type = int, dest = "NEVT", help = "Number of events to be used, default all",
        default = -1
    )
    hyperparamgroup.add_argument(
        "-lr", type = float, dest = "LR", help = "learning rate, default 1e-4", default = 1e-4
    )
    hyperparamgroup.add_argument(
        "-opt", type = str, dest = "OPTIMIZER", help = "optimizer, default adam",
        default = "adam", choices = ["adam", "adadelta", "adagrad", "rmsprop", "sgd"]
    )
    hyperparamgroup.add_argument(
        "-loss", type = str, dest = "LOSS",
        help = "Loss function, default categorical_crossentropy",
        default = "categorical_crossentropy",
        choices = ["mae", "mse", "categorical_crossentropy"]
    )
    hyperparamgroup.add_argument(
        "--activation", type = str, dest = "ACTIVATION", default = "square",
        help = "activation function, default square (Born Machine)",
        choices = ["square", "softmax", "sigmoid"]
    )
    hyperparamgroup.add_argument(
        "-ntfl", type = int, dest = "NTRAINFILES",
        help = "number of files to be used for the training sample, default 1", default = 1
    )
    hyperparamgroup.add_argument(
        "-nvfl", type = int, dest = "NVALFILES",
        help = "number of files to be used for the validation sample, default 1", default = 1
    )

    pathgroup = parser.add_argument_group("Path handling")
    pathgroup.add_argument(
        "-id", "--filename", default = None, dest = "FILENAME", type = str,
        help = "Name of the output folder. "
    )
    pathgroup.add_argument(
        "-dp", "--dataset-path",
        default = "/mt/user-batch/jaraz/RNN_CNN/EnsembleNN/samples_v2", dest = "DATASETPATH",
        type = str, help = "Path of the datasets"
    )
    pathgroup.add_argument(
        "-op", "--output-path", default = "/mt/group-batch/tnc-share/QTN", dest = "OUTPUTPATH",
        type = str, help = "Path for output"
    )
    pathgroup.add_argument(
        "-log", "--log-path", default = "/mt/group-batch/tnc-share/QTN/log_classical",
        dest = "LOGPATH", type = str, help = "Path for tensorboard log"
    )

    args = parser.parse_args()

    if args.FILENAME is None:
        args.FILENAME = "classical_" + args.ANSATZ

    if os.path.isdir(args.OUTPUTPATH):
        ls = [x for x in os.listdir(args.OUTPUTPATH) if args.FILENAME in x]
        if len(ls) == 0:
            args.OUTPUTPATH = os.path.join(args.OUTPUTPATH, args.FILENAME + "_0")
            os.mkdir(args.OUTPUTPATH)
        else:
            f = lambda x: int(x.split("_")[-1])
            ls = max([f(x) for x in ls]) + 1
            args.OUTPUTPATH = os.path.join(args.OUTPUTPATH, args.FILENAME + f"_{ls}")
            os.mkdir(args.OUTPUTPATH)
    else:
        raise FileNotFoundError(args.OUTPUTPATH)

    if not os.path.isdir(os.path.join(args.DATASETPATH, "train")) or not os.path.isdir(
            os.path.join(args.DATASETPATH, "val")
    ):
        raise FileNotFoundError(
            f"Can not find {os.path.join(args.DATASETPATH, 'train')} or"
            f" {os.path.join(args.DATASETPATH, 'val')}"
        )

    log = {"RunType": "ClassicalTNs"}
    for arg, value in sorted(vars(args).items()):
        log[arg] = value
        print("      - " + arg.ljust(25) + f" : {value}")

    with open(os.path.join(args.OUTPUTPATH, "arguments.json"), "w") as out_file:
        json.dump(log, out_file, indent = 4, sort_keys = True)

    main(args)
