#!/usr/bin/env python3
# -*- coding: utf-8 -*-

import argparse
import json
import os
from collections import namedtuple
from typing import NamedTuple

import pennylane as qml
from tqdm import tqdm

os.environ.update({'TF_CPP_MIN_LOG_LEVEL': '3'})

def main(args: NamedTuple, QuantumAnsatz: NamedTuple, ClassicalAnsatz: NamedTuple) :
    import tnqcircuits as tnq
    import tensorflow as tf
    from sklearn.metrics import roc_curve, auc
    import numpy as np
    tf.keras.backend.set_floatx('float64')

    dev_opts = {}
    if "qiskit" in args.DEVICE:
        from qiskit import IBMQ
        from qiskit.providers.aer.noise import NoiseModel

        IBMQ.load_account()
        provider = IBMQ.get_provider(hub = args.HUB, group = args.GROUP, project = "main")
        if "ibmq" in args.DEVICE:
            dev_opts.update({"provider" : provider})

        runtime_backends = provider.backends(input_allowed='runtime')

        print("\n   * Available Backends:")
        for backend in runtime_backends:
            print(
                f"      - Name : {backend.name()}".ljust(45, ".") + \
                f"hub: {backend.hub},  group: {backend.group},  project: {backend.project}"
            )

        if args.NOISEMODEL is not None and args.DEVICE == "qiskit.aer":
            noise_model = NoiseModel.from_backend(provider.get_backend(args.NOISEMODEL))
            dev_opts.update({"noise_model": noise_model})


    #################
    # Preprocessing #
    #################
    test_dict = {
        "sig": [os.path.join(args.DATASETPATH, f"test/tt_CovNet_{x:04d}.npz") for x in
                range(1, min(args.NTESTFILES + 1, 42))],
        "bkg": [os.path.join(args.DATASETPATH, f"test/QCD_CovNet_{x:04d}.npz") for x in
                range(1, min(args.NTESTFILES + 1, 42))]
    }

    processor = {
        "slice_length"      : ClassicalAnsatz.SLICE,
        "training_data_path": os.path.join(args.DATASETPATH, "train"),
        "unravel_mode"      : ClassicalAnsatz.UNRAVEL,
        "return_downsampled": True,
    }
    test_sample = tnq.generators.TNDataGenerator(
        smp_path_dict = test_dict, batch_size = args.NBATCH, shuffle = True,
        dtype = tf.float64, generatortype = "classical", nevt_lim = args.NEVT,
        processor = processor, hilbert_dims = ClassicalAnsatz.HILBERT,
        return_square = (ClassicalAnsatz.TNANSATZ == "ttn"), shuffle_mode = "test",
    )

    #########################
    # Setup Quantum Ansatz  #
    #########################

    if "default" not in args.DEVICE:
        dev = qml.device(
            args.DEVICE, wires = QuantumAnsatz.NQUBITS,
            shots = None if args.SHOTS <= 0 else args.SHOTS,
            backend = args.BACKEND, **dev_opts,
        )
        if "simulator" in args.BACKEND:
            dev.backend.set_options(max_parallel_threads=0)
            dev.backend.set_options(max_parallel_shots=0)
            if args.GPU:
                dev.backend.set_options(device='GPU')
    else:
        dev = qml.device(
            args.DEVICE, wires = QuantumAnsatz.NQUBITS,
            shots = None if args.SHOTS <= 0 else args.SHOTS
        )


    options = {"diff_method" : "parameter-shift", "interface" : "tf"}
    if QuantumAnsatz.ANSATZ == "ttn":
        ansatz, weight_shape = tnq.circuits.ttn(
            dev, nqubits = QuantumAnsatz.NQUBITS, rotation = QuantumAnsatz.ROT, **options
        )
    elif QuantumAnsatz.ANSATZ == "mps":
        ansatz, weight_shape = tnq.circuits.mps(
            dev, nqubits = QuantumAnsatz.NQUBITS, rotation = QuantumAnsatz.ROT,
            pbc = QuantumAnsatz.PBC, **options
        )
    elif QuantumAnsatz.ANSATZ == "mera":
        ansatz, weight_shape = tnq.circuits.mera(
            dev, nqubits = QuantumAnsatz.NQUBITS, rotation = QuantumAnsatz.ROT, **options
        )

    ##############################
    # Setup Classical TN Ansatz  #
    ##############################
    kernel_regularizer = None
    if ClassicalAnsatz.L2REGULARIZATION > 0.:
        kernel_regularizer = tf.keras.regularizers.L2(ClassicalAnsatz.L2REGULARIZATION)

    if ClassicalAnsatz.TNANSATZ == "mps":
        tn_layer = tnq.layers.MPS(
            n_nodes = [test_sample.output_shape[1] // QuantumAnsatz.NQUBITS] * QuantumAnsatz.NQUBITS,
            hilbert_dim = ClassicalAnsatz.HILBERT, bond_dim = ClassicalAnsatz.BOND,
            output_dim = 1, activation = ClassicalAnsatz.ACTIVATION,
            pbc = ClassicalAnsatz.TNPBC, dtype = tf.float64,
            kernel_regularizer = kernel_regularizer,
        )
    elif ClassicalAnsatz.TNANSATZ == "ttn":
        tn_layer = tnq.layers.TTN(
            n_nodes = test_sample.output_shape[1] * test_sample.output_shape[2],
            hilbert_dim = ClassicalAnsatz.HILBERT, bond_dim = ClassicalAnsatz.BOND,
            output_dim = QuantumAnsatz.NQUBITS,
            execute_as = {"mode": "layer", "nqubits": QuantumAnsatz.NQUBITS},
            activation = ClassicalAnsatz.ACTIVATION, dtype = tf.float64,
            kernel_regularizer = kernel_regularizer,
        )
    elif ClassicalAnsatz.TNANSATZ == "mera":
        raise NotImplementedError


    ########
    # Test #
    ########

    for idx in range(args.NTESTS):

        ################
        # Hybrid Model #
        ################

        model = tnq.models.HybridModel(
            tn_layer = tn_layer, qnode = ansatz, weight_shapes = weight_shape,
            output_dim = (2,), weight_specs = {
                "weights": {
                    "initializer": "glorot_uniform", "trainable": True, "dtype": tf.float64,
                }
            }
        )
        model.build(tuple([5] + list(test_sample.output_shape[1:])))
        model.load_weights(os.path.join(args.INPUTPATH, "weights.h5"))

        try:
            yhat, y_truth = -1, -1
            with tqdm(
                    total = len(test_sample) + 1, desc = f"Testing {idx + 1}/{args.NTESTS}",
                    unit = "batch"
            ) as pbar:
                for x, y in test_sample:
                    if isinstance(yhat, int):
                        # Map is a faster parallelized option to run the circuit.
                        # However it brakes the graph structure so can not be used
                        # during training.
                        if args.DEVICE == "qiskit.ibmq":
                            yhat = model(x).numpy()
                        else:
                            yhat = model.map(x).numpy()
                        y_truth = y.numpy()
                    else:
                        if args.DEVICE == "qiskit.ibmq":
                            yhat = np.vstack((yhat, model(x).numpy()))
                        else:
                            yhat = np.vstack((yhat, model.map(x).numpy()))
                        y_truth = np.vstack((y_truth, y.numpy()))
                    pbar.update()

        except KeyboardInterrupt:
            print("   * Test stopped by the user.")

        except Exception as err:
            print("   * Test failed.")
            print(str(err))

        if not isinstance(yhat, int) or not isinstance(y_truth, int):
            filename = (
                "test_results.npz" if args.NTESTS == 1 else f"test_results_{idx:03d}.npz"
            )
            if len(y_truth) > 0 and len(yhat) > 0:
                fpr, tpr, _ = roc_curve(
                    y_truth[:, 0], yhat[:, 0], drop_intermediate = False, pos_label = 1
                )
                np.savez_compressed(
                    os.path.join(QuantumAnsatz.OUTPUTPATH, filename), yhat = yhat,
                    y_truth = y_truth, fpr = fpr, tpr = tpr, auc = auc(fpr, tpr)
                )

    print(f"\n   * Output path: {QuantumAnsatz.OUTPUTPATH}\n")


if __name__ == "__main__" :

    parser = argparse.ArgumentParser(
        description = "Test hybrid classical and quantum Tensor Network-based neural networks"
    )

    ansatz = parser.add_argument_group("Ansatz handling")
    ansatz.add_argument(
        "INPUTPATH", type = str, help = "Path to the folder containing `arguments.json`."
    )

    device = parser.add_argument_group("Device handling")
    device.add_argument(
        "-dev", dest = "DEVICE", default = "qiskit.aer", type = str,
        help = "PennyLane device, default qiskit.aer",
        choices = list(qml.plugin_devices.keys()), )
    device.add_argument(
        "-shots", dest = "SHOTS", default = 5000, type = int, help = "shots, default 5K"
    )
    device.add_argument(
        "-hub", dest = "HUB", default = "ibm-q", type = str,
        help = "IBMQ provider hub, default ibm-q."
    )
    device.add_argument(
        "-group", dest = "GROUP", default = "open", type = str,
        help = "IBMQ provider group, default open."
    )
    device.add_argument(
        "--backend", dest = "BACKEND", default = "aer_simulator", type = str,
        help = "device backend, default aer_simulator",
        choices = ['aer_simulator', 'qasm_simulator', 'ibmq_santiago', 'ibmq_bogota',
                   'ibmq_lima', 'ibmq_belem', 'ibmq_quito', 'ibmq_manila', "ibm_perth",
                   "ibm_lagos", "ibmq_jakarta", "ibmq_casablanca"]
    )
    device.add_argument(
        "-gpu", dest = "GPU", default = False, action = "store_true",
        help = "Set gpu simulator."
    )
    device.add_argument(
        "-noise", "--noise-model", dest = "NOISEMODEL", default = None, type = str,
        choices = ['ibmq_santiago', 'ibmq_bogota', 'ibmq_lima', 'ibmq_belem', 'ibmq_quito',
                   'ibmq_manila', "ibm_perth", "ibm_lagos", "ibmq_jakarta", "ibmq_casablanca"],
        help = "Set noise model for the IBM-Q machine. PS santiago is extremely noisy hence "
               "giving worse results rest is quite similar.",
    )


    hyperparamgroup = parser.add_argument_group("Hyperparameter handling")
    hyperparamgroup.add_argument(
        "-ntfl", type = int, dest = "NTESTFILES",
        help = "number of files to be used for the test sample, default 1", default = 1
    )
    hyperparamgroup.add_argument(
        "-nb", "--nbatch", type = int, dest = "NBATCH",
        help = "Number of events in a single batch, default 100", default = 100,
    )
    hyperparamgroup.add_argument(
        "-nevt", type = int, dest = "NEVT", help = "Number of events to be used, default all",
        default = -1
    )
    hyperparamgroup.add_argument(
        "--ntest", type = int, dest = "NTESTS", default = 1,
        help = "Number of tests to be executed on the same sample. Default 1",
    )


    pathgroup = parser.add_argument_group("Path handling")
    pathgroup.add_argument(
        "-id", "--filename", default = None, dest = "FILENAME", type = str,
        help = "Name of the output folder. "
    )
    pathgroup.add_argument(
        "-dp", "--dataset-path",
        default = "/mt/user-batch/jaraz/RNN_CNN/EnsembleNN/samples_v2", dest = "DATASETPATH",
        type = str, help = "Path of the datasets"
    )


    args = parser.parse_args()

    if args.FILENAME is None:
        args.FILENAME = "test"

    if os.path.isdir(args.INPUTPATH):
        ls = [x for x in os.listdir(args.INPUTPATH) if args.FILENAME in x]
        if len(ls) == 0:
            outputpath = os.path.join(args.INPUTPATH, args.FILENAME + "_0")
            os.mkdir(outputpath)
        else:
            f = lambda x: int(x.split("_")[-1])
            ls = max([f(x) for x in ls]) + 1
            outputpath = os.path.join(args.INPUTPATH, args.FILENAME + f"_{ls}")
            os.mkdir(outputpath)
    else:
        raise FileNotFoundError(args.INPUTPATH)

    if not os.path.isdir(os.path.join(args.DATASETPATH, "test")):
        raise FileNotFoundError(f"Can not find {os.path.join(args.DATASETPATH, 'test')}")

    arguments = os.path.join(args.INPUTPATH, "arguments.json")
    if not os.path.isfile(arguments):
        raise FileNotFoundError(arguments)
    if not os.path.isfile(os.path.join(args.INPUTPATH, "weights.h5")):
        raise FileNotFoundError(os.path.join(args.INPUTPATH, "weights.h5"))

    with open(arguments, "r") as f:
        arguments = json.load(f)


    QuantumAnsatz = namedtuple(
        "QuantumAnsatz", ["ANSATZ", "NQUBITS", "ROT", "PBC", "OUTPUTPATH"]
    )
    ClassicalAnsatz = namedtuple(
        "ClassicalAnsatz",
        ["TNANSATZ", "HILBERT", "ACTIVATION", "BOND", "TNPBC", "L2REGULARIZATION", "SLICE",
         "UNRAVEL", ]
    )

    Qansatz = QuantumAnsatz(
        arguments["QTNANSATZ"], arguments["NQUBITS"], arguments["ROT"], arguments["PBC"],
        outputpath
    )
    Cansatz = ClassicalAnsatz(
        arguments["TNANSATZ"], arguments["HILBERT"], arguments["ACTIVATION"],
        arguments["BOND"], arguments["TNPBC"], arguments["L2REGULARIZATION"],
        arguments["SLICE"], arguments["UNRAVEL"],
    )

    print("   * Arguments:")
    log = {}
    for arg, value in sorted(vars(args).items()) :
        log[arg] = value
        print("      - " + arg.ljust(25) + f" : {value}")

    print("\n   * Quantum Ansatz:")
    for arg, value in Qansatz._asdict().items() :
        print("      - " + arg.ljust(25) + f" : {value}")

    print("\n   * Classical Ansatz:")
    for arg, value in Cansatz._asdict().items() :
        print("      - " + arg.ljust(25) + f" : {value}")


    log.update(Qansatz._asdict())
    log.update(Cansatz._asdict())
    with open(os.path.join(outputpath, "arguments.json"), "w") as out_file :
        json.dump(log, out_file, indent = 4, sort_keys = True)

    main(args, Qansatz, Cansatz)
