# tnqcircuits
[![JHEP](https://img.shields.io/static/v1?style=plastic&label=DOI&message=10.1103/PhysRevA.106.062423&color=blue)](https://journals.aps.org/pra/abstract/10.1103/PhysRevA.106.062423)
[![arxiv](https://img.shields.io/static/v1?style=plastic&label=arXiv&message=2202.10471&color=brightgreen)](https://doi.org/10.48550/arXiv.2202.10471)

[![Python v3.8](https://img.shields.io/badge/python-3670A0?style=plastic&logo=python&logoColor=ffdd54&label=3.7|3.8)]() 
[![TensorFlow](https://img.shields.io/badge/TensorFlow-%23FF6F00.svg?style=plastic&logo=TensorFlow&logoColor=white&label=2.7.0)]() 
[![Qiskit](https://img.shields.io/badge/Qiskit-%236929C4.svg?style=plastic&logo=Qiskit&logoColor=white&label=0.32.1)]() 
[![MIT license](https://img.shields.io/badge/License-MIT-blue.svg?style=plastic)](https://gitlab.com/jackaraz/tnqcircuits/-/blob/main/LICENSE)

[![dataset](https://img.shields.io/static/v1?style=plastic&label=Dataset&message=10.5281/zenodo.2603256&color=blue)](https://doi.org/10.5281/zenodo.2603256)

Tensor Network-based Quantum Machine Learning

## Installation

`$ pip install -e .` or `$ make install`

## Usage

### Tensor Network-based Variational Quantum Circuits

Three different quantum variational circuit has been provided within this package namely MPS, TTN and MERA. Whilst MPS
has been written as a generic function TTN and MERA circuits are limited to 4, 6 and 8 qubits. One can create VQC using
following functions:
```python
import pennylane as qml
import tnqcircuits as tnq

ttn, weight_shape_ttn = tnq.circuits.ttn(
    qml.device("default.qubit", wires = 4), nqubits = 4, rotation = "real"
)
mps, weight_shape_mps = tnq.circuits.mps(
    qml.device("default.qubit", wires = 4), nqubits = 4, rotation = "real", pbc = False
)
mera, weight_shape_mera = tnq.circuits.mera(
    qml.device("default.qubit", wires = 4), nqubits = 4, rotation = "real"
)
```
Each function takes a device, number of qubits and rotation. Rotation indicates the block structure where it can be 
either `real` (builts trainable blocs with RY gates) or `general` (builds trainable blocs via general unitary gates).
MPS has extra `pbc` keyword argument which stands for periodic boundary condition. `weight_shape` is an indicator for 
the shape of trainable parameters that circuit needs.

### Creating a TensorFlow based Quantum Model

Continuing from previous example a quantum model for ML training can be created via `QuantumModel` function.
```python
model = tnq.models.QuantumModel(
    mps, weight_shape_mps, output_dim = (2,), weight_specs = {
        "weights" : {"initializer" : "glorot_uniform", "dtype" : tf.float64}
    }
)
```
This takes QTN ansatz as defined in the previous section, and it requires the weight shape information to create trainable
parameters properly. `weight_specs` simply identifies properties of trainable parameters. Note that for the best performance
in terms of speed, one should use TF based quantum device with backpropagation. 

### Training with Quantum Natural Gradient Decent

TNQCircuits packages includes a TF based implementation of QNGD training algorithm and `QuantumModel` is specially 
designed to be compatible with both QNGD and traditional training algorithms.
```python
model.compile(
    optimizer = tnq.optimizers.QGradientDescent(0.01), # or tf.optimizers.get("adam")
    loss_fn = tf.losses.CategoricalCrossentropy(name = "xentropy"),
    metrics = [tf.metrics.BinaryAccuracy(name = "acc")]
)
model.build((4,))
```
Note that since this is a custom model, all Keras functionality may not be available. Hence inputting proper classes instead
of strings for metrics and optimizers is highly recommended. Note that `fit` function is specifically designed for 
`tf.data.Dataset` or `tf.keras.utils.Sequence`; NumPy inputs wouldn't work however necessary modifcation can easily be 
implemented.

### Training Hybrid models with Mixed SGD and QNGD algorithms

TNQCircuits package is equipped with classical MPS, TTN and MERA layers adapted for fast execution in TF using only SGD
for training. All architectures has certain limitations but the most generic one is again MPS. Please see the documentation 
in the corresponding class for more details.

An MPS layer, for instace, can be created via
```python
tn_layer = tnq.layers.MPS(
            n_nodes = [9,9,9,9],
            hilbert_dim = 2, bond_dim = 10, output_dim = 1,
            activation = "square", pbc = False, dtype = tf.float64,
        )
```
Here `n_nodes` indicates number of nodes per MPS where if an intiger given the network will be assumed as a single MPS, but
a sequence type of input will be interpreted as multpiple independent MPS (9 nodes each for this example). And the final 
result will be concantaneted  to be inputted to a quantum circuit. Activation can be `square` which means mod square of
the network but one can also use `sigmoid` or `relu` activation functions. Using `tn_layer` and one of the QTN ansatz 
defined above, one can form a hybrid network using `HybridModel` function

```python
model = tnq.models.HybridModel(
    tn_layer = tn_layer, qnode = mps, weight_shapes = weight_shape_mps, output_dim = (2,),
    weight_specs = {"weights": {"initializer": "glorot_uniform", "dtype": tf.float64}}
)
model.compile(
    optimizer = tf.optimizers.get("adam"), 
    quantum_optimizer = tnq.optimizers.QGradientDescent(1e-2), # or None
    loss_fn = tf.losses.CategoricalCrossentropy(name = "xentropy"),
    metrics = [tf.metrics.BinaryAccuracy(name = "acc")]
)
```
Here if `quantum_optimizer` is not `None` then a mixed training algorithm will be used where classical layer will be 
trained with SGD and quantum layer will be trained with QNGD. If none then both will be trained with SGD.

### Training classical TNs
Classical TNs are designed purely as Keras layers so one can treat them as such. A Keras model can be written via
```python
model = tf.keras.Sequential(
    [tf.keras.layers.InputLayer((6, 10,), dtype = tf.float64),
     tnq.layers.MPS(
         n_nodes = 6, hilbert_dim = 10, bond_dim = 5, 
         output_dim = 2, pbc = False, activation = "square", 
     )],
    name = "MPS", 
)
```
This model expects input dimensions as `(Nt, 6, 10)` which shows 6 dimensional feature space mapped on 10D hypersphere.
Mapping of course does not need to be on a hypersphere, any orthonormal mapping would do the trick. Rest of the training 
is as usual. One can use TN layers as intermediate layers as well by adjusting output dimensions. Note that only MPS has 
been built to be general, MERA and TTN implementations are limited to what has been used in the paper.

`activation = "square"` means that the output will be the mod square of the ansatz. This can be changed to `relu` or 
`softmax`.

### Scripts that are used in this study

All the scripts that are used for this study can be found in `tnqcircuits/bin` folder. Files starting with `run` are
the training scripts and the ones starting with `test` are testing scripts using the folder formation defined in `run` 
script. In order to reproduce the results presented in the paper one needs to preprocess the dataset where the codes
for preprocessing are provided in a [previous study](https://gitlab.com/jackaraz/EnsembleNN).

## Examples of Tensor Network-based Quantum variational circuits used in this study

In the following images we only used `RY` rotation for the variational node but general `U3` based circuits are 
available as well. In this study we only considered two qubit entanglement at a time but this can be generalized 
there is no restriction in the Tensor Network structure.
### Tree Tensor Network:
<p align="center">
<img src="./docs/images/ttn_6qubits.png" alt="TTN with 6 qubits" style="width:400px;"/>
</p>

### Matrix Product State:
<p align="center">
<img src="./docs/images/mps_6qubits.png" alt="MPS with 6 qubits" style="width:700px;"/>
</p>

### Multiscale Entanglement Renormalization Ansatz:
<p align="center">
<img src="./docs/images/mera_6qubits.png" alt="MERA with 6 qubits" style="width:600px;"/>
</p>

### Possible bugfix requirement in TensorFlow>=2.6.2
- AUC metric under `tf.metrics.AUC` does not work in when data type is `tf.float64`. 
This can be fixed by modifying L382 of `<pythonenv>/lib/python3.8/site-packages/keras/utils/metrics_utils.py`

```python
weights = tf.cast(tf.multiply(sample_weights, label_weights), dtype=y_pred.dtype)
```
Note that without this bugfix `runQTN` wont work. A simpler method is just removing AUC calculation from `runQTN`.
