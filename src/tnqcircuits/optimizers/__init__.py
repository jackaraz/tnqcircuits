from .quantum_grad_descent import QGradientDescent

__all__ = ["QGradientDescent"]