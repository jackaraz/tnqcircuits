from collections.abc import Iterable
import tensorflow as tf
import numpy as np

class QGradientDescent(tf.optimizers.Optimizer):
    """
    https://arxiv.org/pdf/1612.01789.pdf
    """


    def __init__(self, lr: float = 1e-2, **kwargs):
        super(QGradientDescent, self).__init__(**kwargs, name = "QGradientDescent")
        self._set_hyper('learning_rate', lr)


    def apply_gradients(self, grads_vars_metrics: Iterable):
        for grad, var, metric in grads_vars_metrics:
            lr = self._get_hyper("learning_rate", var.dtype)
            try:
                update = lr * tf.linalg.diag_part(
                    tf.linalg.solve(metric, tf.linalg.diag(grad))
                )
            except Exception:
                try:
                    update = tf.convert_to_tensor(
                        lr.numpy() * np.linalg.solve(metric.numpy(), grad.numpy()),
                        dtype = var.dtype
                    )
                except Exception as err:
                    print(
                        "   * failed linalg.solve in both TensorFlow and NumPy "
                        "reducing to gradient descent"
                    )
                    update = lr * grad

            var.assign(var - update)


    def get_config(self) -> dict:
        config = super(QGradientDescent, self).get_config()
        config.update({'learning_rate': self._serialize_hyperparameter('learning_rate')})
        return config