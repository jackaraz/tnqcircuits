from .effective_dimensions import EffectiveDimensions

__all__ = ["EffectiveDimensions"]