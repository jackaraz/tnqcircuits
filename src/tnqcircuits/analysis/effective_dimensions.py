from typing import Sequence

import numpy as np
import tensorflow as tf
from scipy.special import logsumexp
from tqdm import tqdm

from tnqcircuits.models.quantum_model import QuantumModel


class EffectiveDimensions:
    def __init__(
        self,
        ansatz: tf.keras.Model,
        input_shape: Sequence[int],
        output_dims: int = 2,
        x_ntrials: int = 100,
        theta_ntrials: int = 1,
        theta_lims: Sequence[int] = [0, 1],
        x_lims: Sequence[int] = [0, np.pi],
    ):
        """
        Effective dimension calculator
        based on https://github.com/amyami187/effective_dimension

        Parameters
        ----------
        ansatz : tf.keras.Model
            trainable keras model with classical or quantum network
        input_shape : Sequence[int]
            input shape that ansatz should receive
        x_ntrials : int
            Number of trials for input parameters
        theta_ntrials : int
            number of trials for trainable parameters
        theta_lims: Sequence[int]
            lower and upper limits of trainable parameters
        output_dims: int
            output dimensions
        """

        assert isinstance(ansatz, tf.keras.Model), f"Invalid ansatz: {type(ansatz)}"
        self.ansatz = ansatz
        self.input_shape = input_shape
        self.x_ntrials = int(x_ntrials)
        self.theta_ntrials = theta_ntrials
        self.theta_shapes = [x.shape.as_list() for x in ansatz.trainable_variables]
        self.ntheta = sum([np.prod(x) for x in self.theta_shapes])
        self.theta_lims = theta_lims
        self.output_dims = output_dims
        self.x_lims = x_lims

    def get_fisher(self) -> np.ndarray:
        """
        Calculate the Fisher Matrix

        Returns
        -------
        np.ndarray:
            Fisher matrix with shape (theta_ntrials*x_ntrials, ntheta, ntheta)
        """
        tot_fisher = np.zeros((self.ntheta, self.ntheta), dtype=np.float32)
        x_samples = np.random.uniform(*self.x_lims, (self.x_ntrials, *self.input_shape))

        if hasattr(self.ansatz, "normalize"):
            self.ansatz.normalize = False

        is_circuit = isinstance(self.ansatz, QuantumModel)

        with tqdm(total=self.x_ntrials) as pbar:
            for idx, x in enumerate(x_samples):
                x = x.reshape(1, *self.input_shape)
                self.ansatz.set_weights(
                    [np.random.uniform(*self.theta_lims, shape) for shape in self.theta_shapes]
                )

                norm_factor = tf.reshape(
                    tf.reduce_sum(
                        self.ansatz(x_samples) if is_circuit else tf.square(self.ansatz(x_samples)),
                        axis = 0
                    ), (1, 2)
                )

                tmp_grad = []
                with tf.GradientTape() as tape1:
                    with tf.GradientTape() as tape2:
                        yhat = ( self.ansatz(x) if is_circuit else tf.square(self.ansatz(x)) ) / norm_factor
                        logyhat1 = tf.math.log(yhat[:, 0])
                    tmp_grad.append(
                        [
                            tf.reshape(g, (1, tf.reduce_prod(g.shape))) * tf.sqrt(yhat[:, 0])
                            for g in tape2.gradient(logyhat1, self.ansatz.trainable_variables)
                        ]
                    )
                    logyhat2 = tf.math.log(yhat[:, 1])
                tmp_grad.append(
                    [
                        tf.reshape(g, (1, tf.reduce_prod(g.shape))) * tf.sqrt(yhat[:, 1])
                        for g in tape1.gradient(logyhat2, self.ansatz.trainable_variables)
                    ]
                )

                concat_grad = []
                for g1, g2 in zip(*tmp_grad):
                    concat_grad.append(tf.concat([g1, g2], 0))
                current_gradient = tf.concat(concat_grad, 1).numpy()

                tmp_sum = np.zeros((self.output_dims, self.ntheta, self.ntheta))
                for ido in range(self.output_dims):
                    tmp_sum[ido] += np.array(
                        np.outer(current_gradient[ido], np.transpose(current_gradient[ido]))
                    )
                tot_fisher += np.sum(tmp_sum, axis=0)  # ntheta x ntheta
                pbar.update()

        mean_fisher = tot_fisher / float(self.theta_ntrials * self.x_ntrials)
        fisher_trace = np.trace(mean_fisher)
        f_hat = self.ntheta * mean_fisher.reshape(1, self.ntheta, self.ntheta) / fisher_trace

        eigvals, _ = np.linalg.eigh(f_hat.reshape(self.ntheta, self.ntheta))

        return f_hat, eigvals

    # def fisher_hat(self):
    #
    #     fishers = self.get_fisher()
    #     # compute the trace with all fishers
    #     fisher_trace = np.trace(np.average(fishers, axis = 0))
    #     # average the fishers over the num_inputs to get the empirical fishers
    #     fisher = np.average(
    #         np.reshape(fishers, (self.theta_ntrials, self.x_ntrials, self.ntheta, self.ntheta)),
    #         axis = 1
    #     )
    #     # calculate f_hats for all the empirical fishers
    #     f_hat = self.ntheta * fisher / fisher_trace
    #     return f_hat, fisher_trace

    def __call__(self, number_of_samples: Sequence[int], return_mean_fisher_eig_vals: bool = False):
        """
        Calculate normalized effective dimensions for given number of examples

        Parameters
        ----------
        number_of_samples :

        Returns
        -------

        """
        effective_dimensions = []

        f_hat, eig_vals = self.get_fisher()

        for nsmp in number_of_samples:
            try:
                Fhat = f_hat * nsmp / (2 * np.pi * np.log(nsmp))
                one_plus_F = np.eye(self.ntheta) + Fhat
                det = np.linalg.slogdet(one_plus_F)[1]  # log det because of overflow
                r = det / 2  # divide by 2 because of sqrt
                effective_dimensions.append(
                    2
                    * (logsumexp(r) - np.log(self.theta_ntrials))
                    / np.log(nsmp / (2 * np.pi * np.log(nsmp)))
                )
            except Exception:
                effective_dimensions.append(0.0)

        if return_mean_fisher_eig_vals:
            return np.array(effective_dimensions) / self.ntheta, eig_vals

        return np.array(effective_dimensions) / self.ntheta
