from typing import Text

import pennylane as qml
from qiskit import QuantumCircuit, QuantumRegister, ClassicalRegister

from .utils import general_unitary, real_unitary


def mps(
    device: qml.Device, nqubits: int = 4, rotation: Text = "real", pbc: bool = False, **kwargs
):
    """
    Matrix Product State variational quantum circuit

    Parameters
    ----------

    device : qml.Device
        PennyLane device for execution
    nqubits : int
        number of qubits
    rotation : Text
        rotation implementation of each tensor node: real (rotation around y-axis only)
        or general (unitary rotation U3)
    pbc: bool
        periodic boundary conditions
    kwargs:
        keyword arguments for quantum node

    Returns
    -------
    circuit function and weight shape
    """
    assert device.num_wires == nqubits, "Given number of qubits needs to match with the device."

    if rotation == "real":

        @qml.qnode(device, **kwargs)
        def circuit(inputs, weights):
            qml.AngleEmbedding(inputs, wires = range(nqubits), rotation = "Y")

            idw = 0
            for iq in range(nqubits - 1):
                real_unitary(weights[idw:idw + 2], iq, iq + 1)
                idw += 2

            if pbc:
                real_unitary(weights[idw:idw + 2], iq + 1, 0)
                return qml.probs(op = qml.PauliZ(0))

            return qml.probs(op = qml.PauliZ(iq + 1))


        weight_shape = {"weights": ((nqubits - 1) * 2,) if not pbc else (nqubits * 2,)}

    elif rotation == "general":

        @qml.qnode(device, **kwargs)
        def circuit(inputs, weights):
            qml.AngleEmbedding(inputs, wires = range(nqubits), rotation = "Y")

            idw = 0
            for iq in range(nqubits - 1):
                general_unitary(weights[idw:idw + 6], iq, iq + 1)
                idw += 6

            if pbc:
                general_unitary(weights[idw:idw + 6], iq + 1, 0)
                return qml.probs(op = qml.PauliZ(0))

            return qml.probs(op = qml.PauliZ(iq + 1))


        weight_shape = {"weights": ((nqubits - 1) * 6,) if not pbc else (nqubits * 6,)}

    else:
        raise NotImplemented(f"{rotation} has not been implemented.")

    return circuit, weight_shape


def mps_4qubit_qiskit(rotation: Text = "real"):
    """

    Parameters
    ----------
    rotation : Text
        rotation implementation of each tensor node: real (rotation around y-axis only)
        or general (unitary rotation U3)

    Returns
    -------
    circuit function and weight shape
    """

    if rotation == "real":

        def circuit(inputs, weights):
            circuit = QuantumCircuit(
                QuantumRegister(4, "q"), ClassicalRegister(1, "c"), name="mps_real"
            )

            for wire, inpt in enumerate(inputs):
                circuit.ry(inpt, wire)

            circuit.ry(weights[0], 0)
            circuit.ry(weights[1], 1)
            circuit.cnot(0,1)

            circuit.ry(weights[2], 1)
            circuit.ry(weights[3], 2)
            circuit.cnot(1,2)

            circuit.ry(weights[4], 2)
            circuit.ry(weights[5], 3)
            circuit.cnot(2,3)

            circuit.measure(3,0)

            return circuit

        weight_shape = {"weights" : (6,)}

    elif rotation == "general":

        def circuit(inputs, weights):
            circuit = QuantumCircuit(
                QuantumRegister(4, "q"), ClassicalRegister(1, "c"), name="mps_general"
            )

            for wire, inpt in enumerate(inputs):
                circuit.ry(inpt, wire)

            circuit.u(weights[0], weights[1], weights[2], 0)
            circuit.u(weights[3], weights[4], weights[5], 1)
            circuit.cnot(0,1)

            circuit.u(weights[6], weights[7], weights[8], 1)
            circuit.u(weights[9], weights[10], weights[11], 2)
            circuit.cnot(1,2)

            circuit.u(weights[12], weights[13], weights[14], 2)
            circuit.u(weights[15], weights[16], weights[17], 3)
            circuit.cnot(2,3)

            circuit.measure(3,0)

            return circuit

        weight_shape = {"weights" : (18,)}

    else:
        raise NotImplemented(f"{rotation} has not been implemented.")

    return circuit, weight_shape