from typing import Text

import pennylane as qml
from qiskit import QuantumCircuit, QuantumRegister, ClassicalRegister

from .utils import general_unitary, real_unitary


def ttn(device: qml.Device, nqubits: int = 4, rotation: Text = "real", **kwargs):
    """
    Tree Tensor Network based variational quantum circuit.

    Parameters
    ----------
    device : qml.Device
        PennyLane device for execution
    nqubits : int
        number of qubits. Only 4, 6 and 8 are available
    rotation : Text
        rotation implementation of each tensor node: real (rotation around y-axis only)
        or general (unitary rotation U3)
    kwargs:
        keyword arguments for quantum node

    Returns
    -------
    circuit function and weight shape
    """
    assert device.num_wires == nqubits, "Given number of qubits needs to match with the device."
    assert nqubits in [4, 6, 8, 16], f"Implementation for {nqubits} qubits is not available."
    if nqubits == 16:
        assert rotation == "real", f"{rotation} implementation for 16 qubits is not available"

    if rotation == "real":
        if nqubits == 4:
            @qml.qnode(device, **kwargs)
            def circuit(inputs, weights):
                qml.AngleEmbedding(inputs, wires = range(4), rotation = "Y")

                real_unitary(weights[:2], 0, 1)
                real_unitary(weights[2:4], 2, 3, True)
                real_unitary(weights[4:6], 1, 2)

                return qml.probs(op = qml.PauliZ(2))


            weight_shape = {"weights": (6,)}

        elif nqubits == 6:
            @qml.qnode(device, **kwargs)
            def circuit(inputs, weights):
                qml.AngleEmbedding(inputs, wires = range(6), rotation = "Y")

                real_unitary(weights[:2], 0, 1)
                real_unitary(weights[2:4], 2, 3, True)
                real_unitary(weights[4:6], 4, 5, True)

                real_unitary(weights[6:8], 1, 2)
                # real_unitary(weights[8:10], 3, 4, True)

                # real_unitary(weights[10:12], 2, 3)
                real_unitary(weights[8:10], 2, 4)

                return qml.probs(op = qml.PauliZ(4))


            weight_shape = {"weights": (10,)}

        elif nqubits == 8:
            @qml.qnode(device, **kwargs)
            def circuit(inputs, weights):
                qml.AngleEmbedding(inputs, wires = range(8), rotation = "Y")

                real_unitary(weights[:2], 0, 1)
                real_unitary(weights[2:4], 2, 3, True)
                real_unitary(weights[4:6], 4, 5)
                real_unitary(weights[6:8], 6, 7, True)

                real_unitary(weights[8:10], 1, 2)
                # real_unitary(weights[10:12], 3, 4, True)
                # real_unitary(weights[12:14], 5, 6, True)
                real_unitary(weights[10:12], 5, 6, True)

                # real_unitary(weights[14:16], 2, 3)
                # real_unitary(weights[16:18], 4, 5, True)

                # real_unitary(weights[18:20], 3, 4)
                real_unitary(weights[12:14], 2, 5)

                return qml.probs(op = qml.PauliZ(5))


            weight_shape = {"weights": (14,)}

        elif nqubits == 16:

            @qml.qnode(device, **kwargs)
            def circuit(inputs, weights):
                qml.AngleEmbedding(inputs, wires = range(16), rotation = "Y")

                real_unitary(weights[:2], 0, 1)
                real_unitary(weights[2:4], 2, 3, True)
                real_unitary(weights[4:6], 4, 5)
                real_unitary(weights[6:8], 6, 7, True)
                real_unitary(weights[8:10], 8, 9)
                real_unitary(weights[10:12], 10, 11, True)
                real_unitary(weights[12:14], 12, 13)
                real_unitary(weights[14:16], 14, 15, True)

                real_unitary(weights[16:18], 1, 2)
                real_unitary(weights[18:20], 5, 6, True)
                real_unitary(weights[20:22], 9, 10)
                real_unitary(weights[22:24], 13, 14, True)

                real_unitary(weights[24:26], 2, 5)
                real_unitary(weights[26:28], 10, 13, True)

                real_unitary(weights[28:30], 5, 10)

                return qml.probs(op = qml.PauliZ(10))

            weight_shape = {"weights": (30,)}

    elif rotation == "general":
        if nqubits == 4:
            @qml.qnode(device, **kwargs)
            def circuit(inputs, weights):
                qml.AngleEmbedding(inputs, wires = range(4), rotation = "Y")

                general_unitary(weights[:6], 0, 1)
                general_unitary(weights[6:12], 3, 2)
                general_unitary(weights[12:18], 1, 2)

                return qml.probs(op = qml.PauliZ(2))


            weight_shape = {"weights": (18,)}

        elif nqubits == 6:
            @qml.qnode(device, **kwargs)
            def circuit(inputs, weights):
                qml.AngleEmbedding(inputs, wires = range(6), rotation = "Y")

                general_unitary(weights[:6], 0, 1)
                general_unitary(weights[6:12], 3, 2)
                general_unitary(weights[12:18], 5, 4)

                general_unitary(weights[18:24], 1, 2)
                # general_unitary(weights[24:30], 4, 3)

                # general_unitary(weights[30:36], 2, 3)
                general_unitary(weights[24:30], 2, 4)

                return qml.probs(op = qml.PauliZ(4))


            weight_shape = {"weights": (30,)}

        elif nqubits == 8:
            @qml.qnode(device, **kwargs)
            def circuit(inputs, weights):
                qml.AngleEmbedding(inputs, wires = range(8), rotation = "Y")

                general_unitary(weights[:6], 0, 1)
                general_unitary(weights[6:12], 3, 2)
                general_unitary(weights[12:18], 4, 5)
                general_unitary(weights[18:24], 7, 6)

                general_unitary(weights[24:30], 1, 2)
                # general_unitary(weights[30:36], 4, 3)
                # general_unitary(weights[36:42], 6, 5)
                general_unitary(weights[30:36], 6, 5)

                # general_unitary(weights[42:48], 2, 3)
                # general_unitary(weights[48:54], 5, 4)

                # general_unitary(weights[54:60], 3, 4)
                general_unitary(weights[36:42], 2, 5)

                return qml.probs(op = qml.PauliZ(5))


            weight_shape = {"weights": (42,)}


    else:
        raise NotImplemented(f"{rotation} has not been implemented.")

    return circuit, weight_shape


def ttn_4qubit_qiskit(rotation: Text = "real"):
    """

    Parameters
    ----------
    rotation : Text
        rotation implementation of each tensor node: real (rotation around y-axis only)
        or general (unitary rotation U3)

    Returns
    -------
    circuit function and weight shape
    """
    assert device.num_wires == 4, "This circuit only uses 4 qubits"

    if rotation == "real":

        def circuit(inputs, weights):
            circuit = QuantumCircuit(
                QuantumRegister(4, "q"), ClassicalRegister(1, "c"), name = "ttn_real"
            )

            for wire, inpt in enumerate(inputs):
                circuit.ry(inpt, wire)

            circuit.ry(weights[0], 0)
            circuit.ry(weights[1], 1)
            circuit.cnot(0, 1)

            circuit.ry(weights[2], 2)
            circuit.ry(weights[3], 3)
            circuit.cnot(3, 2)

            circuit.ry(weights[4], 1)
            circuit.ry(weights[5], 2)
            circuit.cnot(1, 2)

            circuit.measure(2, 0)

            return circuit


        weight_shape = {"weights": (6,)}

    elif rotation == "general":

        def circuit(inputs, weights):
            circuit = QuantumCircuit(
                QuantumRegister(4, "q"), ClassicalRegister(1, "c"), name = "ttn_general"
            )

            for idx, i in enumerate(inputs):
                circuit.ry(i, idx)

            circuit.u(weights[0], weights[1], weights[2], 0)
            circuit.u(weights[3], weights[4], weights[5], 1)
            circuit.cnot(0, 1)

            circuit.u(weights[6], weights[7], weights[8], 2)
            circuit.u(weights[9], weights[10], weights[11], 3)
            circuit.cnot(3, 2)

            circuit.u(weights[12], weights[13], weights[14], 1)
            circuit.u(weights[15], weights[16], weights[17], 2)
            circuit.cnot(1, 2)

            circuit.measure(2, 0)

            return circuit


        weight_shape = {"weights": (18,)}

    else:
        raise NotImplemented(f"{rotation} has not been implemented.")

    return circuit, weight_shape
