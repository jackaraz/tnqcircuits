import pennylane as qml
from pennylane import numpy as np

from qiskit import QuantumCircuit, QuantumRegister, ClassicalRegister

from .utils import general_unitary, real_unitary
from typing import Text


def mera(device: qml.Device, nqubits: int = 4, rotation: Text = "real", **kwargs):
    """
    Multiscale entanglement renormalization ansatz based variational quantum circuit

    Parameters
    ----------
    device : qml.Device
        PennyLane device for execution
    nqubits : int
        number of qubits. Only 4, 6 and 8 are available
    rotation : Text
        rotation implementation of each tensor node: real (rotation around y-axis only)
        or general (unitary rotation U3)
    kwargs:
        keyword arguments for quantum node

    Returns
    -------
    circuit function and weight shape
    """
    assert device.num_wires == nqubits, "Given number of qubits needs to match with the device."
    assert nqubits in [4, 6, 8, 16], f"Implementation for {nqubits} qubits is not available."
    if nqubits == 16:
        assert rotation == "real", f"{rotation} implementation for 16 qubits is not available"

    if rotation == "real":
        if nqubits == 4:

            @qml.qnode(device, **kwargs)
            def circuit(inputs, weights):
                qml.AngleEmbedding(inputs, wires=range(4), rotation="Y")

                real_unitary(weights[:2], 1, 2)
                real_unitary(weights[2:4], 0, 1)
                real_unitary(weights[4:6], 3, 2)
                real_unitary(weights[6:8], 1, 2)

                return qml.probs(op=qml.PauliZ(2))

            weight_shape = {"weights": (8,)}

        elif nqubits == 6:

            @qml.qnode(device, **kwargs)
            def circuit(inputs, weights):
                qml.AngleEmbedding(inputs, wires=range(6), rotation="Y")

                real_unitary(weights[:2], 1, 2)
                real_unitary(weights[2:4], 3, 4, True)

                real_unitary(weights[4:6], 0, 1)
                real_unitary(weights[6:8], 2, 3)
                real_unitary(weights[8:10], 4, 5, True)

                real_unitary(weights[12:14], 1, 4, True)

                real_unitary(weights[14:16], 1, 2, True)
                real_unitary(weights[16:18], 3, 4)

                real_unitary(weights[18:20], 1, 4)

                return qml.probs(op=qml.PauliZ(4))

            weight_shape = {"weights": (20,)}

        elif nqubits == 8:

            @qml.qnode(device, **kwargs)
            def circuit(inputs, weights):
                qml.AngleEmbedding(inputs, wires=range(8), rotation="Y")

                real_unitary(weights[:2], 1, 2)
                real_unitary(weights[2:4], 3, 4, True)
                real_unitary(weights[4:6], 5, 6)

                real_unitary(weights[6:8], 0, 1)
                real_unitary(weights[8:10], 2, 3, True)
                real_unitary(weights[10:12], 4, 5)
                real_unitary(weights[12:14], 6, 7, True)

                real_unitary(weights[14:16], 2, 5)

                real_unitary(weights[16:18], 1, 2)
                real_unitary(weights[18:20], 5, 6, True)

                real_unitary(weights[20:22], 2, 5, True)

                return qml.probs(op=qml.PauliZ(2))

            weight_shape = {"weights": (22,)}

        elif nqubits == 16:

            @qml.qnode(device, **kwargs)
            def circuit(inputs, weights):
                qml.AngleEmbedding(inputs, wires=range(16), rotation="Y")

                real_unitary(weights[:2], 1, 2)
                real_unitary(weights[2:4], 3, 4, True)
                real_unitary(weights[4:6], 5, 6)
                real_unitary(weights[6:8], 7, 8, True)
                real_unitary(weights[8:10], 9, 10)
                real_unitary(weights[10:12], 11, 12, True)
                real_unitary(weights[12:14], 13, 14)

                # 1st TTN block
                real_unitary(weights[14:16], 0, 1)
                real_unitary(weights[16:18], 2, 3, True)
                real_unitary(weights[18:20], 4, 5)
                real_unitary(weights[20:22], 6, 7, True)
                real_unitary(weights[22:24], 8, 9)
                real_unitary(weights[24:26], 10, 11, True)
                real_unitary(weights[26:28], 12, 13)
                real_unitary(weights[28:30], 14, 15, True)

                # MERA layer
                real_unitary(weights[30:32], 2, 5)
                real_unitary(weights[32:34], 6, 9, True)
                real_unitary(weights[34:36], 10, 13)

                # 2nd TTN block
                real_unitary(weights[36:38], 1, 2)
                real_unitary(weights[38:40], 5, 6, True)
                real_unitary(weights[40:42], 9, 10)
                real_unitary(weights[42:44], 13, 14, True)

                # MERA layer
                real_unitary(weights[44:46], 0, 2)
                real_unitary(weights[46:48], 5, 10, True)
                real_unitary(weights[48:50], 13, 15)

                # 3rd TTN block
                real_unitary(weights[50:52], 2, 5)
                real_unitary(weights[52:54], 10, 13, True)

                # MERA layer
                real_unitary(weights[54:56], 0, 5)
                real_unitary(weights[56:58], 10, 15, True)

                # 4th TTN block
                real_unitary(weights[58:60], 5, 10)

                return qml.probs(op=qml.PauliZ(10))

            weight_shape = {"weights": (60,)}

    elif rotation == "general":
        if nqubits == 4:

            @qml.qnode(device, **kwargs)
            def circuit(inputs, weights):
                qml.AngleEmbedding(inputs, wires=range(4), rotation="Y")

                general_unitary(weights[:6], 1, 2)
                general_unitary(weights[6:12], 0, 1)
                general_unitary(weights[12:18], 3, 2)
                general_unitary(weights[18:24], 1, 2)

                return qml.probs(op=qml.PauliZ(2))

            weight_shape = {"weights": (24,)}

        elif nqubits == 6:

            @qml.qnode(device, **kwargs)
            def circuit(inputs, weights):
                qml.AngleEmbedding(inputs, wires=range(6), rotation="Y")

                general_unitary(weights[:6], 1, 2)
                general_unitary(weights[6:12], 3, 4, True)

                general_unitary(weights[12:18], 0, 1)
                general_unitary(weights[18:24], 2, 3)
                general_unitary(weights[24:30], 4, 5, True)

                general_unitary(weights[30:36], 1, 4, True)

                general_unitary(weights[36:42], 1, 2, True)
                general_unitary(weights[42:48], 3, 4)

                general_unitary(weights[48:54], 1, 4)

                return qml.probs(op=qml.PauliZ(4))

            weight_shape = {"weights": (54,)}

        elif nqubits == 8:

            @qml.qnode(device, **kwargs)
            def circuit(inputs, weights):
                qml.AngleEmbedding(inputs, wires=range(8), rotation="Y")

                general_unitary(weights[:6], 1, 2)
                general_unitary(weights[6:12], 3, 4, True)
                general_unitary(weights[12:18], 5, 6)

                general_unitary(weights[18:24], 0, 1)
                general_unitary(weights[24:30], 2, 3, True)
                general_unitary(weights[30:36], 4, 5)
                general_unitary(weights[36:42], 6, 7, True)

                general_unitary(weights[42:48], 2, 5)

                general_unitary(weights[48:54], 1, 2)
                general_unitary(weights[54:60], 5, 6, True)

                general_unitary(weights[60:66], 2, 5, True)

                return qml.probs(op=qml.PauliZ(2))

            weight_shape = {"weights": (66,)}

    else:
        raise NotImplemented(f"{rotation} has not been implemented.")

    return circuit, weight_shape


def mera_4qubit_qiskit(rotation: Text = "real"):
    """

    Parameters
    ----------
    rotation : Text
        rotation implementation of each tensor node: real (rotation around y-axis only)
        or general (unitary rotation U3)

    Returns
    -------
    circuit function and weight shape
    """
    assert device.num_wires == 4, "This circuit only uses 4 qubits"

    if rotation == "real":

        def circuit(inputs, weights):
            circuit = QuantumCircuit(
                QuantumRegister(4, "q"), ClassicalRegister(1, "c"), name="mera_real"
            )

            for wire, inpt in enumerate(inputs):
                circuit.ry(inpt, wire)

            circuit.ry(weights[0], 1)
            circuit.ry(weights[1], 2)
            circuit.cnot(1, 2)

            circuit.ry(weights[2], 0)
            circuit.ry(weights[3], 1)
            circuit.cnot(0, 1)

            circuit.ry(weights[4], 2)
            circuit.ry(weights[5], 3)
            circuit.cnot(3, 2)

            circuit.ry(weights[6], 1)
            circuit.ry(weights[7], 2)
            circuit.cnot(1, 2)

            circuit.measure(2, 0)

            return circuit

        weight_shape = {"weights": (8,)}

    elif rotation == "general":

        def circuit(inputs, weights):
            circuit = QuantumCircuit(
                QuantumRegister(4, "q"), ClassicalRegister(1, "c"), name="mera_general"
            )

            for wire, inpt in enumerate(inputs):
                circuit.ry(inpt, wire)

            circuit.u(weights[0], weights[1], weights[2], 1)
            circuit.u(weights[3], weights[4], weights[5], 2)
            circuit.cnot(1, 2)

            circuit.u(weights[6], weights[7], weights[8], 0)
            circuit.u(weights[9], weights[10], weights[11], 1)
            circuit.cnot(0, 1)

            circuit.u(weights[12], weights[13], weights[14], 2)
            circuit.u(weights[15], weights[16], weights[17], 3)
            circuit.cnot(3, 2)

            circuit.u(weights[18], weights[19], weights[20], 1)
            circuit.u(weights[21], weights[22], weights[23], 2)
            circuit.cnot(1, 2)

            circuit.measure(2, 0)

            return circuit

        weight_shape = {"weights": (24,)}

    else:
        raise NotImplemented(f"{rotation} has not been implemented.")

    return circuit, weight_shape
