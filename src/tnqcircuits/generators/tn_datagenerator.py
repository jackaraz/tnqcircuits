import os
from typing import Text, Callable, Optional, Union, Tuple

import numpy as np
import tensorflow as tf

from .utils import preprocess, get_scale, downsample, hypersphere, unravel

np.warnings.filterwarnings("ignore", category=np.VisibleDeprecationWarning)


class custom_processor_setup:
    """
    Create custom image preprocessor
    Parameters
    ----------
    slice_length : int
        the length of slicing to reduce dimensionality of 37x37 pixel image
    training_data_path : Text
        path of the training dataset.
    unravel_mode : Text
        how to unravel the image. Options are reshape, eta-based, phi-based
    return_downsampled: bool
        downsample the image after cropping
    min_max_lims: Tuple[float, float]
        upper and lower limit for min_max scaler
    """

    def __init__(
        self,
        slice_length: int,
        training_data_path: Text,
        unravel_mode: Text = "reshape",
        scale: np.ndarray = None,
        return_downsampled: bool = True,
        min_max_lims: Tuple[float, float] = (0, np.pi),
        cherry_pick: Optional[Tuple[int]] = None,
    ):
        if scale is None:
            self.scale = get_scale(
                slice_length=slice_length,
                training_data_path=training_data_path,
                unravel_mode=unravel_mode,
                return_downsampled=return_downsampled,
                min_max_lims=min_max_lims,
            )
        else:
            self.scale = scale
        self.unravel_mode = unravel_mode
        self.slice_length = slice_length
        self.downsample = return_downsampled
        self.cherry_pick = cherry_pick

    def processor(self, image: np.ndarray) -> np.ndarray:
        if self.downsample:
            image = downsample(
                image[
                    :,
                    self.slice_length : image.shape[1] - self.slice_length,
                    self.slice_length : image.shape[-1] - self.slice_length,
                ]
            )
        else:
            image = image[
                :,
                self.slice_length : image.shape[1] - self.slice_length,
                self.slice_length : image.shape[-1] - self.slice_length,
            ]

        image = unravel(image, self.unravel_mode) * self.scale

        if self.cherry_pick is not None and not callable(self.cherry_pick):
            return image[:, self.cherry_pick]
        if callable(self.cherry_pick):
            return self.cherry_pick(image)

        return image

    def __call__(self, image: np.ndarray, **kwargs):
        return self.processor(image)


class TNDataGenerator(tf.keras.utils.Sequence):
    """
    Data Generator for classical and quantum Tensor Network based Network

    Parameters
    ----------
    smp_path_dict : dict
        sample path dictionary {"sig" : [], "bkg" : []}
    batch_size : int
    nqubits : int
    shuffle : bool
    dtype : tf.DType
    generatortype : Text
        "classical" or "quantum"
    processor : Optional[Union[Callable, dict]]
        image processor takes input data and nqubits keyword argument
        returns shape (Nt, x-pixels, y-pixels)
        if processor is a dictionarry it requires two keyword arguments slice_length: int,
         training_data_path: Text, unravel_mode: Text.
         see `tnq.generators.tn_datagenerator.custom_processor_setup` for more details.
    nevt_lim : int
        max number of events. Only available when there is one file for sig and bkg. default -1
    hilbert_dims: int
        Hilbert space dimensions, default 2
    return_square: bool
        return squared image instead of a one dimensional array. Default False
    shuffle_mode: Text
        Generator mode. default train.
        options :
            train: maximize shuffling (if shuffle is true)
            test: only shuffle once (if shuffle is true) does not work if there are
                  multiple files
    kwargs :
        keyword arguments for tf.keras.utils.Sequence
    """

    def __init__(
        self,
        smp_path_dict: dict,
        batch_size: int = 100,
        nqubits: int = 4,
        shuffle=True,
        dtype: tf.DType = tf.float64,
        generatortype: Text = "classical",
        processor: Optional[Union[Callable, dict]] = preprocess,
        nevt_lim: int = -1,
        hilbert_dims: int = 2,
        return_square: bool = False,
        shuffle_mode: Text = "train",
        bkg_only: bool = False,
        sig_only: bool = False,
        **kwargs,
    ):
        super(TNDataGenerator, self).__init__(**kwargs)

        assert shuffle_mode in [
            "test",
            "train",
        ], f"Unknown shuffle mode: {shuffle_mode}"

        sort = lambda x: int(x.split("_")[-1].split(".")[0])

        self.sig_paths = [x for x in smp_path_dict["sig"] if os.path.isfile(x)]
        self.sig_paths.sort(key=sort)
        self.bkg_paths = [x for x in smp_path_dict["bkg"] if os.path.isfile(x)]
        self.bkg_paths.sort(key=sort)
        self.nqubits = nqubits
        self.dtype = dtype
        self.shuffle = shuffle
        self.return_square = return_square
        self.shuffle_mode = shuffle_mode

        self.bkg_only = bkg_only
        self.sig_only = sig_only
        self.on_epoch_end()

        assert generatortype in [
            "classical",
            "c",
            "classic",
            "quantum",
            "q",
        ], f"Unknown generator type : {generatortype}"

        self.is_classical = generatortype in ["classical", "c", "classic"]
        self.dataset = None
        self.hilbert_dim = hilbert_dims

        self.phi = lambda image: hypersphere(image, self.hilbert_dim)

        if callable(processor):
            self.processor = processor

        elif isinstance(processor, dict):
            self.nqubits = False
            self.processor = custom_processor_setup(**processor)

        self.nevents = 0
        for fl in self.sig_paths:
            self.nevents += np.load(fl)["shape"][0]
        for fl in self.bkg_paths:
            self.nevents += np.load(fl)["shape"][0]

        if len(self.sig_paths) == 1 and len(self.bkg_paths) == 1 and nevt_lim > 0:
            self.nevents = nevt_lim
            self.nevt_lim = nevt_lim

        if self.nevents == 0 or len(self.sig_paths) == 0 or len(self.bkg_paths) == 0:
            raise ValueError("Invalid arguments please check sample path dictionary.")

        self.batch_size = batch_size

    def on_epoch_end(self):
        self.samples = [(sig, bkg) for sig, bkg in zip(self.sig_paths, self.bkg_paths)]
        self.batches = []
        self._nfile = 0
        if self.shuffle:
            np.random.shuffle(self.samples)

    def __len__(self):
        return (self.nevents // self.batch_size) - 1

    @property
    def output_shape(self):
        if self.nqubits:
            return tuple(
                [self.batch_size, self.nqubits]
                + (self.is_classical) * [self.hilbert_dim]
            )
        else:
            if self.return_square:
                assert int(np.sqrt(len(self.processor.scale))) ** 2 == len(
                    self.processor.scale
                ), "Please check the requirements"
                return (
                    self.batch_size,
                    int(np.sqrt(len(self.processor.scale))),
                    int(np.sqrt(len(self.processor.scale))),
                    self.hilbert_dim,
                )
            return tuple(
                [self.batch_size, len(self.processor.scale)]
                + self.is_classical * [self.hilbert_dim]
            )

    def __iter__(self):
        self.on_epoch_end()
        for i in range(len(self) + 1):
            yield self[i]

    def __getitem__(self, item):
        if len(self.batches) == 0 or self.dataset is None:
            self._read_data()

        current_batch = self.batches[0]
        self.batches.remove(current_batch)

        return tf.convert_to_tensor(
            self.dataset[0][current_batch], dtype=self.dtype
        ), tf.convert_to_tensor(self.dataset[1][current_batch], dtype=tf.int32)

    def _read_data(self):
        if len(self.samples) > 1 or self.dataset is None:
            sigfl, bkgfl = self.samples[self._nfile]
            self._nfile += 1
            sig_image = np.load(sigfl)["sample"]
            bkg_image = np.load(bkgfl)["sample"]

            y = []
            if hasattr(self, "nevt_lim"):
                if not self.bkg_only:
                    sig_image = sig_image[: self.nevt_lim // 2]
                    bkg_image = bkg_image[: self.nevt_lim // 2]
                    dataset = self.processor(
                        np.vstack((sig_image, bkg_image)), n_qubits=self.nqubits
                    )
                    y.extend([[1, 0]] * sig_image.shape[0])
                    y.extend([[0, 1]] * bkg_image.shape[0])
                elif self.bkg_only:
                    bkg_image = bkg_image[: self.nevt_lim]
                    dataset = self.processor(bkg_image, n_qubits=self.nqubits)
                    y.extend([[0, 1]] * bkg_image.shape[0])
                elif self.sig_only:
                    sig_image = sig_image[: self.nevt_lim // 2]
                    dataset = self.processor(sig_image, n_qubits=self.nqubits)
                    y.extend([[1, 0]] * sig_image.shape[0])

            y = np.array(y)

            self.batches = np.array_split(
                np.arange(y.shape[0]), max(y.shape[0] // self.batch_size, 1)
            )
            if self.shuffle:
                idx = list(range(y.shape[0]))
                if self.shuffle_mode == "train":
                    np.random.shuffle(self.batches)
                np.random.shuffle(idx)
                y = y[idx]
                dataset = dataset[idx]

            if self.is_classical and self.hilbert_dim >= 2:
                dataset = tf.stack(self.phi(dataset), axis=-1).numpy()

            if self.return_square:
                if self.is_classical and self.hilbert_dim >= 2:
                    dataset = dataset.reshape(
                        dataset.shape[0],
                        int(np.sqrt(dataset.shape[1])),
                        int(np.sqrt(dataset.shape[1])),
                        dataset.shape[2],
                    )
                else:
                    dataset = dataset.reshape(
                        dataset.shape[0],
                        int(np.sqrt(dataset.shape[1])),
                        int(np.sqrt(dataset.shape[1])),
                    )

            self.dataset = (dataset, y)

        else:
            x, y = self.dataset
            self.batches = np.array_split(
                np.arange(y.shape[0]), y.shape[0] // self.batch_size
            )
            if self.shuffle and self.shuffle_mode == "train":
                idx = list(range(y.shape[0]))
                np.random.shuffle(self.batches)
                np.random.shuffle(idx)
                x = x[idx]
                y = y[idx]
                self.dataset = (x, y)
