import os
from math import factorial
from typing import Text, Union, Sequence, Tuple

import numpy as np
import tensorflow as tf
from sklearn.preprocessing import MinMaxScaler


def downsample(image, pad = False):
    padded = image
    if pad:
        padded = np.pad(padded, pad_width = ((0, 0), (1, 1), (1, 1)), mode = 'constant')
    rolled1 = np.roll(padded, shift = (0, -1, -1), axis = (0, 1, 2))
    rolled2 = np.roll(padded, shift = (0, 0, -1), axis = (0, 1, 2))
    rolled3 = np.roll(padded, shift = (0, -1, 0), axis = (0, 1, 2))
    ave = (padded + rolled1 + rolled2 + rolled3) / 4

    return ave[:, 1:-1:2, 1:-1:2]


def preprocess(image: np.ndarray, n_qubits: int, sl: int = 14) -> np.ndarray:
    """
    Crop and downsample the given set of images and fit the pixel values between (0,pi). The
    Scale of the fitting has been precalculated with respect to mixed 200K events.

    Parameters
    ----------
    image : np.ndarray
    n_qubits : int
        number of qubits
    sl : int
        initial crop size

    Returns
    -------
    np.ndarray
    """
    image = downsample(
        image[:, sl:image.shape[1] - sl, sl:image.shape[-1] - sl]
    )  # [:, :, 1:3]
    if n_qubits > 4:
        image = image[:, :, 1:3]
    image = image.reshape(-1, np.prod(image.shape[1:]))[:, :n_qubits]
    if n_qubits == 8:
        scale = np.array(
            [0.02414905, 0.02991002, 0.019471, 0.02393889, 0.03937127, 0.04085571, 0.04004872,
             0.04178571]
        )
    elif n_qubits == 6:
        scale = np.array(
            [0.02414905, 0.02991002, 0.019471, 0.02393889, 0.03937127, 0.04085571]
        )
    elif n_qubits == 4:
        scale = np.array([0.02414905, 0.02991002, 0.019471, 0.02393889])
    return image * scale


def get_scale(
        slice_length: int, training_data_path: Text, unravel_mode: Text = "reshape",
        return_downsampled = True, min_max_lims: Tuple[float, float] = (0, np.pi),
) -> np.ndarray:
    """
    Get standardization scale of the batched image. Current reshaping is based on standard
    reshaping. The scale is calculated for 200K events for signal and background (equally divided).
    Parameters
    ----------
    slice_length : int
        the length of slicing to reduce dimensionality of 37x37 pixel image
    training_data_path : Text
        path of the training dataset.
    unravel_mode: Text
        how to unravel the image. Options are reshape, eta-based, phi-based
    min_max_lims: Tuple[float, float]
        upper and lower limit for min_max scaler

    Returns
    -------
    np.ndarray
        scale for the data
    """
    tt = np.load(os.path.join(training_data_path, "tt_CovNet_0001.npz"))["sample"]
    qcd = np.load(os.path.join(training_data_path, "QCD_CovNet_0001.npz"))["sample"]
    for idx in range(2, 21):
        if not os.path.isfile(os.path.join(training_data_path, f"tt_CovNet_{idx:04d}.npz")):
            break
        tt = np.vstack(
            (tt,
             np.load(os.path.join(training_data_path, f"tt_CovNet_{idx:04d}.npz"))["sample"])
        )
        qcd = np.vstack(
            (qcd,
             np.load(os.path.join(training_data_path, f"QCD_CovNet_{idx:04d}.npz"))["sample"])
        )

    if return_downsampled:
        sliced_tt = downsample(
            tt[:, slice_length:tt.shape[1] - slice_length,
            slice_length:tt.shape[-1] - slice_length]
        )
        sliced_qcd = downsample(
            qcd[:, slice_length:qcd.shape[1] - slice_length,
            slice_length:qcd.shape[-1] - slice_length]
        )
    else:
        sliced_tt = tt[:, slice_length:tt.shape[1] - slice_length,
                    slice_length:tt.shape[-1] - slice_length]
        sliced_qcd = qcd[:, slice_length:qcd.shape[1] - slice_length,
                     slice_length:qcd.shape[-1] - slice_length]

    sliced_tt, sliced_qcd = unravel([sliced_tt, sliced_qcd], unravel_mode)

    data = np.vstack([sliced_tt, sliced_qcd])
    scaler = MinMaxScaler(feature_range = min_max_lims)
    X = scaler.fit_transform(data)

    return scaler.scale_


def unravel(
        images: Union[np.ndarray, Sequence[np.ndarray]], mode: Text = "reshape",
) -> Union[np.ndarray, Sequence[np.ndarray]]:
    """
    Unravel given image according to the unravel mode
    Parameters
    ----------
    images : Union[np.ndarray, Sequence[np.ndarray]]
    mode : Text
        how to unravel the image. Options are reshape, eta-based, phi-based

    Returns
    -------
    Union[np.ndarray, Sequence[np.ndarray]]
        reshaped images
    """
    if isinstance(images, np.ndarray):
        if mode == "reshape":
            images = images.reshape(-1, np.prod(images.shape[1:]))

        elif mode == "eta-based":
            image_tmp = []
            for image in images:
                im = []
                for i in range(image.shape[0]):
                    if i % 2 == 0:
                        for j in range(image.shape[1]):
                            im.append(image[i, j])
                    else:
                        for j in reversed(range(image.shape[1])):
                            im.append(image[i, j])
                image_tmp.append(im)

            images = np.array(image_tmp)

        elif mode == "phi-based":
            image_tmp = []
            for image in images:
                im = []
                for i in range(image.shape[1]):
                    if i % 2 == 0:
                        for j in range(image.shape[0]):
                            im.append(image[j, i])
                    else:
                        for j in reversed(range(image.shape[0])):
                            im.append(image[j, i])
                image_tmp.append(im)
            images = np.array(image_tmp)

        else:
            raise ValueError(f"Unknown unravel mode: {mode}")

        return images

    elif isinstance(images, (list, tuple)) and len(images) == 2:
        sliced_tt, sliced_qcd = images
        if mode == "reshape":
            sliced_tt = sliced_tt.reshape(-1, np.prod(sliced_tt.shape[1:]))
            sliced_qcd = sliced_qcd.reshape(-1, np.prod(sliced_tt.shape[1:]))

        elif mode == "eta-based":
            tt_tmp, qcd_tmp = [], []
            for image_tt, image_qcd in zip(sliced_tt, sliced_qcd):
                im_tt, im_qcd = [], []
                for i in range(image_tt.shape[0]):
                    if i % 2 == 0:
                        for j in range(image_tt.shape[1]):
                            im_tt.append(image_tt[i, j])
                            im_qcd.append(image_qcd[i, j])
                    else:
                        for j in reversed(range(image_tt.shape[1])):
                            im_tt.append(image_tt[i, j])
                            im_qcd.append(image_qcd[i, j])
                tt_tmp.append(im_tt)
                qcd_tmp.append(im_qcd)
            sliced_tt = np.array(tt_tmp)
            sliced_qcd = np.array(qcd_tmp)

        elif mode == "phi-based":
            tt_tmp, qcd_tmp = [], []
            for image_tt, image_qcd in zip(sliced_tt, sliced_qcd):
                im_tt, im_qcd = [], []
                for i in range(image_tt.shape[0]):
                    if i % 2 == 0:
                        for j in range(image_tt.shape[1]):
                            im_tt.append(image_tt[j, i])
                            im_qcd.append(image_qcd[j, i])
                    else:
                        for j in reversed(range(image_tt.shape[1])):
                            im_tt.append(image_tt[j, i])
                            im_qcd.append(image_qcd[j, i])
                tt_tmp.append(im_tt)
                qcd_tmp.append(im_qcd)
            sliced_tt = np.array(tt_tmp)
            sliced_qcd = np.array(qcd_tmp)

        else:
            raise ValueError(f"Unknown unravel mode: {mode}")

        return [sliced_tt, sliced_qcd]


def hypersphere(x, dim):
    binom_coeff = lambda a, b: factorial(a) / (factorial(b) * factorial(a - b))
    return [
        tf.cast(tf.math.sqrt(binom_coeff(dim - 1, d - 1)), dtype = x.dtype) \
        * tf.math.cos(x * np.pi * 0.5) ** (dim - d) * tf.math.sin(x * np.pi * 0.5) ** (d - 1)
        for d in range(1, dim + 1)
    ]
