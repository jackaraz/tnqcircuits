import tensorflow as tf
from tensorflow.python.keras.engine import compile_utils
from tensorflow.python.keras.utils import tf_utils
from tensorflow.python.profiler import trace

tf.keras.backend.set_floatx('float64')
tf.get_logger().setLevel("ERROR")

import pennylane as qml
from pennylane import numpy as np

from tnqcircuits.layers import QuantumLayer
from tnqcircuits.optimizers import QGradientDescent

from typing import Sequence, Text, Union
import os, copy

os.environ['TF_CPP_MIN_LOG_LEVEL'] = '3'


class QuantumModel(tf.keras.Model):
    """
    Interface to train the given quantum circuit

    Parameters
    ----------
    qnode : pennylane.QNode
        Quantum circuit
    weight_shapes : dict
        dictionary for weight shapes {"weights" : <shape>}
    output_dim : Sequence[int]
        output dimensions
    weight_specs : dict
        input for weight initialization
    kwargs :
        keyword arguments for keras model
    """


    def __init__(
            self, qnode: qml.QNode, weight_shapes: dict, output_dim: Sequence[int],
            weight_specs: dict, **kwargs, ):
        super(QuantumModel, self).__init__(**kwargs, name = "QuantumModel")

        assert isinstance(qnode, qml.QNode), "Circuit needs to be a QNode object"

        self.metric_fn = qml.metric_tensor(qnode, "diag")
        self.qlayer = QuantumLayer(
            qnode, weight_shapes, output_dim, weight_specs, name = "QuantumLayer"
        )


    def build(self, input_shape):
        self.qlayer.build(input_shape)
        super().build(input_shape)


    def compile(
            self, loss_fn = tf.losses.BinaryCrossentropy(), optimizer = QGradientDescent(1e-2),
            metrics: Sequence[tf.metrics.Metric] = None
    ) -> None:
        self.loss = compile_utils.LossesContainer([loss_fn])
        self.loss.build(0.8)
        self.optimizer = optimizer
        if metrics == None:
            self.compiled_metrics = compile_utils.MetricsContainer([])
        else:
            self.compiled_metrics = compile_utils.MetricsContainer(metrics)
        self.compiled_metrics.build(0.8, 1)


    def train_on_batch(self, x, y):
        with tf.GradientTape() as tape:
            if isinstance(self.optimizer, QGradientDescent):
                # https://github.com/PennyLaneAI/pennylane/issues/2040
                metric_tensor = self.metric_fn(
                    x[0], self.qlayer.trainable_variables[0]
                )
            yhat = self.qlayer(x)
            loss = self.loss(y, yhat)
        gradients = tape.gradient(loss, self.qlayer.trainable_variables)
        if isinstance(self.optimizer, QGradientDescent):
            self.optimizer.apply_gradients(
                zip(gradients, self.qlayer.trainable_variables, metric_tensor)
            )
        else:
            self.optimizer.apply_gradients(zip(gradients, self.qlayer.trainable_variables))
        self.compiled_metrics.update_state(y, yhat)
        return loss, yhat


    def test_on_batch(self, x, y):
        yhat = self.qlayer(x)
        loss = self.loss(y, yhat)
        self.compiled_metrics.update_state(y, yhat)
        return loss, yhat


    @property
    def logs(self):
        {
            **{loss.name: loss.result() for loss in self.loss.metrics},
            **{metric.name: metric.result() for metric in self.compiled_metrics.metrics}
        }


    def get_cuttent_log(self, title: Text = "train"):
        txt = ""
        for key, item in {
            **{loss.name: loss.result() for loss in self.loss.metrics},
            **{metric.name: metric.result() for metric in self.compiled_metrics.metrics}
        }.items():
            txt += f"{title}_{key} = {item:.5f}, "
        return txt


    def metric_results(self):
        return {metric.name: metric.result().numpy() for metric in
                self.compiled_metrics.metrics}


    def reset_loss(self):
        self.loss.reset_state()


    def reset_metrics(self):
        self.compiled_metrics.reset_state()


    def on_epoch_end(self):
        self.reset_metrics()
        self.reset_loss()


    def call(self, inputs: tf.Tensor) -> tf.Tensor:
        return self.qlayer(inputs)


    def map(self, inputs: tf.Tensor) -> tf.Tensor:
        return self.qlayer.map(inputs)


    def save_weights(self, filename: Text, **kwargs) -> None:
        w = self.get_weights()
        import h5py
        with h5py.File(filename, "w") as hf:
            weights = hf.create_group("weights")
            weights.create_dataset("weights", data = w[0], compression = "gzip")


    def load_weights(self, filename: Text) -> None:
        import h5py
        with h5py.File(filename, "r") as hf:
            weights = hf.get("weights")
            new_weights = np.array(weights.get("weights"))
        self.set_weights([new_weights])


    def fit(
            self, nepochs: int,
            training_samples: Union[tf.data.Dataset, tf.keras.utils.Sequence],
            validation_samples: Union[tf.data.Dataset, tf.keras.utils.Sequence] = None,
            callbacks: Sequence[tf.keras.callbacks.Callback] = None,
    ):
        """
        Train the model with respect to the given data.

        Parameters
        ----------
        training_samples : Union[tf.data.Dataset, tf.keras.utils.Sequence]
            training samples returns batched x and y values as a tuple
        nepochs : int
            number of epochs
        validation_samples : Union[tf.data.Dataset, tf.keras.utils.Sequence]
            validation samples returns batched x and y values as a tuple
        callbacks : Sequence[tf.keras.callbacks.Callback]
        """

        logs = {}; val_logs = {}

        callbacks = [] if callbacks is None else callbacks
        callbacks = tf.keras.callbacks.CallbackList(
            callbacks, add_history = True, add_progbar = True, model = self, verbose = True,
            epochs = nepochs, steps = len(training_samples) + \
                                      (isinstance(training_samples, tf.keras.utils.Sequence)),
        )
        callbacks.on_train_begin()
        self.stop_training = False

        tensorboard = None
        if any([isinstance(x, tf.keras.callbacks.TensorBoard) for x in callbacks.callbacks]):
            tensorboard = callbacks.callbacks[
                [isinstance(x, tf.keras.callbacks.TensorBoard) for x in
                 callbacks.callbacks].index(True)]

        try:
            for epoch in range(nepochs):
                self.on_epoch_end()
                callbacks.on_epoch_begin(epoch)

                for nbatch, (model_inputs) in enumerate(training_samples):
                    with trace.Trace(
                            'train', epoch_num = epoch, step_num = nbatch,
                            batch_size = model_inputs[0].shape[0], _r = 1
                    ):
                        callbacks.on_train_batch_begin(nbatch)
                        loss, prediction = self.train_on_batch(*model_inputs)
                        logs.update(
                            {
                                **{"loss": loss.result() for loss in self.loss.metrics},
                                **{metric.name: metric.result() for metric in
                                   self.compiled_metrics.metrics}
                            }
                        )
                        callbacks.on_train_batch_end(nbatch , logs)
                        if self.stop_training:
                            break

                logs = tf_utils.sync_to_numpy_or_python_type(logs)
                epoch_logs = copy.copy(logs)
                if tensorboard:
                    with tensorboard._writers["train"].as_default():
                        tf.summary.histogram(
                            "weights", self.trainable_variables[0], step = epoch
                        )

                self.on_epoch_end()
                if validation_samples is not None:
                    callbacks.on_test_begin()
                    for nbatch, (model_inputs) in enumerate(validation_samples):
                        with trace.Trace(
                                'test', epoch_num = epoch, step_num = nbatch,
                                batch_size = model_inputs[0].shape[0], _r = 1
                        ):
                            callbacks.on_test_batch_begin(nbatch)
                            loss, prediction = self.test_on_batch(*model_inputs)
                            val_logs.update(
                                {
                                    **{"loss": loss.result() for loss in self.loss.metrics},
                                    **{metric.name: metric.result() for metric in
                                       self.compiled_metrics.metrics}
                                }
                            )
                            callbacks.on_test_batch_end(nbatch , val_logs)

                    val_logs = tf_utils.sync_to_numpy_or_python_type(val_logs)
                    callbacks.on_test_end(logs = val_logs)
                    epoch_logs.update({"val_" + key: item for key, item in val_logs.items()})

                callbacks.on_epoch_end(epoch, logs = epoch_logs)
                training_logs = epoch_logs
                if self.stop_training:
                    print("   * Training finished early.")
                    break

        except KeyboardInterrupt:
            print("   * Training stopped by the user.")

        callbacks.on_train_end(logs = training_logs)
        return self.history
