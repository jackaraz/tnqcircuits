import tensorflow as tf
import numpy as np
from tensorflow.python.keras import backend

class ReduceQLROnPlateau(tf.keras.callbacks.ReduceLROnPlateau):
    def __init__(self, **kwargs):
        super(ReduceQLROnPlateau, self).__init__(**kwargs)

    def on_epoch_end(self, epoch, logs=None):
        if self.model.quantum_optimizer is not None:
            logs = logs or {}
            logs['qlr'] = backend.get_value(self.model.quantum_optimizer.lr)
            current = logs.get(self.monitor)

            if self.in_cooldown():
                self.cooldown_counter -= 1
                self.wait = 0

            if self.monitor_op(current, self.best):
                self.best = current
                self.wait = 0
            elif not self.in_cooldown():
                self.wait += 1
                if self.wait >= self.patience:
                    old_lr = backend.get_value(self.model.quantum_optimizer.lr)
                    if old_lr > np.float32(self.min_lr):
                        new_lr = old_lr * self.factor
                        new_lr = max(new_lr, self.min_lr)
                        backend.set_value(self.model.quantum_optimizer.lr, new_lr)
                        if self.verbose > 0:
                            print('\nEpoch %05d: ReduceQLROnPlateau reducing learning '
                                  'rate to %s.' % (epoch + 1, new_lr))
                        self.cooldown_counter = self.cooldown
                        self.wait = 0