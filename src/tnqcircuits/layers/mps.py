from typing import Text, Sequence, Optional

import tensorflow as tf


@tf.keras.utils.register_keras_serializable(package = 'tnqcircuits')
class MPS(tf.keras.layers.Layer):
    """
    Matrix Product State layer.

    Parameters
    ----------
    n_nodes : int
        number of qubits
    hilbert_dim : int
        Hilbert space dimensions
    bond_dim : int
        bond dimensions
    output_dim : int
        output dimensions
    activation : Text
        activation function
    pbc : bool
        periodic boundary conditions
    initializer : Text
        initializer for nodes
    kwargs :
        other keyword arguments for a keras layer
    """


    def __init__(
            self, n_nodes: int, hilbert_dim: int, bond_dim: int, output_dim: int,
            activation: Text = "square", pbc: bool = False,
            kernel_regularizer: Optional[Text] = None, initializer: Text = "glorot_uniform",
            **kwargs
    ):
        super(MPS, self).__init__(**kwargs)
        assert isinstance(n_nodes, (int, tuple, list)), "Unknown n_nodes type."
        self.n_nodes = n_nodes
        self.hilbert_dim = hilbert_dim
        self.bond_dim = bond_dim
        self.output_dim = output_dim
        self.pbc = pbc
        self.activation = (
            tf.keras.activations.get(activation) if activation != "square" else tf.square
        )
        self.kernel_regularizer = tf.keras.regularizers.get(kernel_regularizer)
        self.initializer = tf.initializers.get(initializer)

        self.normalize = True if activation == "square" else False


    def build(self, input_shape: Sequence) -> None:
        super().build(input_shape)
        setattr(self, "nodes", list())

        if isinstance(self.n_nodes, int):
            for inode in range(self.n_nodes):
                if inode == 0:
                    shape = (self.bond_dim if self.pbc else 1, self.hilbert_dim, self.output_dim,
                             self.bond_dim)
                else:
                    if inode == self.n_nodes - 1 and not self.pbc:
                        shape = (self.bond_dim, self.hilbert_dim, 1, 1)
                    else:
                        shape = (self.bond_dim, self.hilbert_dim, 1, self.bond_dim)

                self.nodes.append(
                    self.add_weight(
                        shape = shape, name = f"node_{inode}", trainable = True,
                        initializer = self.initializer, dtype = self.dtype,
                        regularizer=self.kernel_regularizer)
                )

        elif isinstance(self.n_nodes, (tuple, list)):
            for nnode in self.n_nodes:
                self.nodes.append([])
                for inode in range(nnode):
                    if inode == 0:
                        shape = (self.bond_dim if self.pbc else 1, self.hilbert_dim,
                                 self.output_dim, self.bond_dim)
                    else:
                        if inode == nnode - 1 and not self.pbc:
                            shape = (self.bond_dim, self.hilbert_dim, 1, 1)
                        else:
                            shape = (self.bond_dim, self.hilbert_dim, 1, self.bond_dim)

                    self.nodes[-1].append(
                        self.add_weight(
                            shape = shape, name = f"node_{inode}_block_{len(self.nodes)}",
                            trainable = True, initializer = self.initializer,
                            dtype = self.dtype, regularizer=self.kernel_regularizer
                        )
                    )


    def call(self, inputs: tf.Tensor):
        if len(tf.shape(inputs)) == 1:
            inputs = tf.expand_dims(inputs, axis = 0)
        yhat = contraction(
            inputs, self.nodes, pbc = self.pbc,
            is_block = isinstance(self.n_nodes, (list, tuple)),
            output_dims = \
                self.output_dim if isinstance(self.output_dim, int) else self.output_dim[0]
        )
        if self.normalize:
            yhat, _ = tf.linalg.normalize(yhat, axis = -1)
            yhat = tf.where(
                tf.math.is_nan(yhat), tf.convert_to_tensor(0.5, dtype = yhat.dtype), yhat
            )
        return self.activation(yhat)


    def get_config(self):
        config = super().get_config()

        config.update(
            {
                "n_nodes"    : self.n_nodes, "hilbert_dim": self.hilbert_dim, "pbc": self.pbc,
                "bond_dim"   : self.bond_dim, "output_dim": self.output_dim,
                "activation" : tf.keras.utils.serialize_keras_object(self.activation),
                "initializer": tf.keras.utils.serialize_keras_object(self.initializer),
            }
        )
        return config


def compute(
        inputs: Sequence[tf.Tensor], nodes: Sequence[tf.Tensor], pbc: bool = True,
        output_dims: int = 2,
) -> tf.Tensor:
    """
    Contract MPS

    Parameters
    ----------
    inputs : Sequence[tf.Tensor]
        unstacked input tensor with shape [(Hilbert dims), ]
    nodes : Sequence[tf.Tensor]
        MPS nodes
    pbc : bool
        Periodic boundary condition
    output_dims : int
        output dimensions

    Returns
    -------
    tf.Tensor
        contracted result
    """
    for idx, (x, t) in enumerate(zip(inputs, nodes)):
        if idx == 0:
            left = tf.squeeze(tf.tensordot(x, t, axes = [0, 1]))
        else:
            tmp = tf.tensordot(x, t, axes = [0, 1])
            if pbc:
                left = tf.squeeze(tf.einsum("lor,rmn->lomn", left, tmp))
            else:
                if output_dims > 1:
                    left = tf.squeeze(tf.einsum("or,rmn->omn", left, tmp))
                else:
                    left = tf.squeeze(tf.einsum("r,rmn->mn", left, tmp))

    result = left
    if pbc:
        result = tf.einsum("iji->j", result)
    return result


def contraction_switch(
    inputs: tf.Tensor, nodes: Sequence[tf.Tensor], pbc: bool = True, is_block: bool = False,
    output_dims: int = 2,
) -> tf.Tensor:
    """
    switches between regular MPS execution and block form

    Parameters
    ----------
    inputs : tf.Tensor
        input examples
    nodes : Sequence[tf.Tensor]
        trainable MPS tensors
    pbc : bool
        periodic boundary condition
    is_block : bool
        is in block form
    output_dims : int
        output dimension of each MPS

    Returns
    -------
    tf.Tensor
    """
    x = tf.unstack(inputs)
    if is_block:
        blocks = []; idx = 0
        for subnodes in nodes:
            blocks.append(compute(x[idx:idx + len(subnodes)], subnodes, pbc, output_dims))
            idx += len(subnodes)
        blocks = [tf.expand_dims(x, 0) for x in blocks]
        return tf.concat(blocks, 0)

    return compute(x, nodes, pbc, output_dims)



@tf.function
def contraction(
        inputs: tf.Tensor, nodes: Sequence[tf.Tensor], pbc: bool = True,
        is_block: bool = False, output_dims: int = 2,
) -> tf.Tensor:
    return tf.vectorized_map(
        lambda vec: contraction_switch(vec, nodes, pbc, is_block, output_dims), inputs
    )
