from typing import Text, Sequence, Optional, Tuple, List

import tensorflow as tf


@tf.keras.utils.register_keras_serializable(package = 'tnqcircuits')
class MERA(tf.keras.layers.Layer):
    """
    MERA layer.

    Parameters
    ----------
    n_nodes : int
        number of qubits
    hilbert_dim : int
        Hilbert space dimensions
    bond_dim : int
        bond dimensions
    output_dim : int
        output dimensions
    activation : Text
        activation function
    pbs : bool
        periodic boundary conditions
    initializer : Text
        initializer for nodes
    kwargs :
        other keyword arguments for a keras layer
    """


    def __init__(
            self, n_nodes: int, hilbert_dim: int, bond_dim: int, output_dim: int,
            activation: Text = "square", initializer: Text = "glorot_uniform",
            kernel_regularizer: Optional[Text] = None, **kwargs
    ):
        super(MERA, self).__init__(**kwargs)
        assert n_nodes in [4, 6, 8, 16], f"{n_nodes} number of nodes have not been implemented."
        self.n_nodes = n_nodes
        self.hilbert_dim = hilbert_dim
        self.bond_dim = bond_dim
        self.output_dim = output_dim
        self.activation = (
            tf.keras.activations.get(activation) if activation != "square" else tf.square
        )

        self.initializer = tf.initializers.get(initializer)
        self.kernel_regularizer = tf.keras.regularizers.get(kernel_regularizer)

        self.normalize = True if activation == "square" else False


    def build(self, input_shape: Sequence[int]) -> None:
        super().build(input_shape)

        self.layers = []
        if self.n_nodes == 4:
            self.layers.append(
                [self.add_weight(
                    shape = (self.hilbert_dim, self.hilbert_dim, self.bond_dim, self.bond_dim),
                    name = f"l1n1", trainable = True, initializer = self.initializer,
                    dtype = self.dtype, )]
            )

            self.layers.append(
                [self.add_weight(
                    shape = (self.hilbert_dim, self.bond_dim, self.bond_dim),
                    name = f"l2n1", trainable = True, initializer = self.initializer,
                    dtype = self.dtype, ),
                self.add_weight(
                    shape = (self.hilbert_dim, self.bond_dim, self.bond_dim),
                    name = f"l2n2", trainable = True, initializer = self.initializer,
                    dtype = self.dtype, ),
                ]
            )

            self.layers.append(
                [self.add_weight(
                    shape = (self.bond_dim, self.bond_dim, self.output_dim),
                    name = f"l3n1", trainable = True, initializer = self.initializer,
                    dtype = self.dtype, )]
            )

        elif self.n_nodes == 6:
            # VERSION 1
            # self.layers.append(
            #     [self.add_weight(
            #         shape = (self.hilbert_dim, self.hilbert_dim, self.bond_dim, self.bond_dim),
            #         name = f"l1n1", trainable = True, initializer = self.initializer,
            #         dtype = self.dtype, ),
            #     self.add_weight(
            #         shape = (self.hilbert_dim, self.hilbert_dim, self.bond_dim, self.bond_dim),
            #         name = f"l1n2", trainable = True, initializer = self.initializer,
            #         dtype = self.dtype, )
            #     ]
            # )
            #
            # self.layers.append(
            #     [self.add_weight(
            #         shape = (self.hilbert_dim, self.bond_dim, self.bond_dim),
            #         name = f"l2n1", trainable = True, initializer = self.initializer,
            #         dtype = self.dtype, ),
            #     self.add_weight(
            #         shape = (self.bond_dim, self.bond_dim, self.bond_dim),
            #         name = f"l2n2", trainable = True, initializer = self.initializer,
            #         dtype = self.dtype, ),
            #     self.add_weight(
            #         shape = (self.hilbert_dim, self.bond_dim, self.bond_dim),
            #         name = f"l2n3", trainable = True, initializer = self.initializer,
            #         dtype = self.dtype, ),
            #     ]
            # )
            #
            # self.layers.append(
            #     [self.add_weight(
            #         shape = (self.bond_dim, self.bond_dim, self.bond_dim),
            #         name = f"l3n1", trainable = True, initializer = self.initializer,
            #         dtype = self.dtype, )]
            # )
            #
            # self.layers.append(
            #     [self.add_weight(
            #         shape = (self.bond_dim, self.bond_dim, self.output_dim),
            #         name = f"l4n1", trainable = True, initializer = self.initializer,
            #         dtype = self.dtype, )]
            # )
            # VERSION 2:
            first_layer = [
                self.add_weight(
                    shape = (self.hilbert_dim, self.hilbert_dim, self.bond_dim, self.bond_dim),
                    name = f"l0n1", trainable = True, initializer = self.initializer,
                    dtype = self.dtype, regularizer = self.kernel_regularizer,
                ), self.add_weight(
                    shape = (self.hilbert_dim, self.hilbert_dim, self.bond_dim, self.bond_dim),
                    name = f"l0n2", trainable = True, initializer = self.initializer,
                    dtype = self.dtype, regularizer = self.kernel_regularizer,
                )
            ]

            second_layer = [
                self.add_weight(
                    shape = (self.hilbert_dim, self.bond_dim, self.bond_dim),
                    name = f"l1n1", trainable = True, initializer = self.initializer,
                    dtype = self.dtype, regularizer = self.kernel_regularizer,
                ), self.add_weight(
                    shape = (self.bond_dim, self.bond_dim, self.bond_dim, self.bond_dim),
                    name = f"l1n2", trainable = True, initializer = self.initializer,
                    dtype = self.dtype, regularizer = self.kernel_regularizer,
                ), self.add_weight(
                    shape = (self.hilbert_dim, self.bond_dim, self.bond_dim),
                    name = f"l1n3", trainable = True, initializer = self.initializer,
                    dtype = self.dtype, regularizer = self.kernel_regularizer,
                )
            ]

            third_layer = [
                self.add_weight(
                    shape = (self.bond_dim, self.bond_dim, self.bond_dim),
                    name = f"l2n1", trainable = True, initializer = self.initializer,
                    dtype = self.dtype, regularizer = self.kernel_regularizer,
                ), self.add_weight(
                    shape = (self.bond_dim, self.bond_dim, self.bond_dim),
                    name = f"l2n2", trainable = True, initializer = self.initializer,
                    dtype = self.dtype, regularizer = self.kernel_regularizer,
                )
            ]

            output_layer = [
                self.add_weight(
                    shape = (self.bond_dim, self.bond_dim, self.output_dim),
                    name = f"l3n1", trainable = True, initializer = self.initializer,
                    dtype = self.dtype, regularizer = self.kernel_regularizer,
                )
            ]

            self.layers = [first_layer, second_layer, third_layer, output_layer]

        elif self.n_nodes == 8:
            self.layers.append(
                [self.add_weight(
                    shape = (self.hilbert_dim, self.hilbert_dim, self.bond_dim, self.bond_dim),
                    name = f"l1n{i}", trainable = True, initializer = self.initializer,
                    dtype = self.dtype, ) for i in range(3)]
            )

            self.layers.append(
                [self.add_weight(
                    shape = (self.hilbert_dim if i in [0, 3] else self.bond_dim, self.bond_dim,
                             self.bond_dim), name = f"l2n{i}", trainable = True,
                    initializer = self.initializer, dtype = self.dtype, ) for i in range(4)]
            )

            self.layers.append(
                [self.add_weight(
                    shape = (self.bond_dim, self.bond_dim, self.bond_dim),
                    name = f"l3n1", trainable = True, initializer = self.initializer,
                    dtype = self.dtype, )]
            )

            self.layers.append(
                [self.add_weight(
                    shape = (self.bond_dim, self.bond_dim, self.bond_dim),
                    name = f"l4n1", trainable = True, initializer = self.initializer,
                    dtype = self.dtype, )]
            )

            self.layers.append(
                [self.add_weight(
                    shape = (self.bond_dim, self.bond_dim, self.output_dim),
                    name = f"l5n1", trainable = True, initializer = self.initializer,
                    dtype = self.dtype, )]
            )

        elif self.n_nodes == 16:
            first_mera_layer = [
                self.add_weight(
                    shape = (self.hilbert_dim, self.hilbert_dim, self.bond_dim, self.bond_dim),
                    name = f"first_mera_layer_{idx}", trainable = True,
                    initializer = self.initializer, dtype = self.dtype,
                    regularizer = self.kernel_regularizer
                    )
                for idx in range(7)
            ]

            def shape(idx: int) -> Tuple[int, int, int]:
                if idx == 0:
                    return (self.hilbert_dim, self.bond_dim, self.bond_dim)
                elif idx == 7:
                    return (self.bond_dim, self.hilbert_dim, self.bond_dim)

                return (self.bond_dim, self.bond_dim, self.bond_dim)

            first_layer = [
                self.add_weight(
                    shape = shape(idx),
                    name = f"first_layer_{idx}", trainable = True,
                    initializer = self.initializer, dtype = self.dtype,
                    regularizer = self.kernel_regularizer
                    )
                for idx in range(8)
            ]

            second_mera_layer = [
                self.add_weight(
                        shape = (self.bond_dim, self.bond_dim, self.bond_dim, self.bond_dim),
                        name = f"second_mera_layer_{idx}", trainable = True,
                        initializer = self.initializer, dtype = self.dtype,
                        regularizer = self.kernel_regularizer
                )
                for idx in range(3)
            ]

            second_layer = [
                self.add_weight(
                        shape = (self.bond_dim, self.bond_dim, self.bond_dim),
                        name = f"second_layer_{idx}", trainable = True,
                        initializer = self.initializer, dtype = self.dtype,
                        regularizer = self.kernel_regularizer
                )
                for idx in range(4)
            ]

            third_mera_layer = [
                self.add_weight(
                        shape = (self.bond_dim, self.bond_dim, self.bond_dim, self.bond_dim),
                        name = f"third_mera_layer_0", trainable = True,
                        initializer = self.initializer, dtype = self.dtype,
                        regularizer = self.kernel_regularizer
                )
            ]

            third_layer = [
                self.add_weight(
                        shape = (self.bond_dim, self.bond_dim, self.bond_dim),
                        name = f"third_layer_{idx}", trainable = True,
                        initializer = self.initializer, dtype = self.dtype,
                        regularizer = self.kernel_regularizer
                )
                for idx in range(2)
            ]

            forth_layer = [
                self.add_weight(
                        shape = (self.bond_dim, self.bond_dim, self.output_dim),
                        name = f"forth_layer_0", trainable = True,
                        initializer = self.initializer, dtype = self.dtype,
                        regularizer = self.kernel_regularizer
                )
            ]

            self.layers = [first_mera_layer, first_layer, second_mera_layer, second_layer,
                           third_mera_layer, third_layer, forth_layer]

    def call(self, inputs: tf.Tensor):
        if len(tf.shape(inputs)) == 1:
            inputs = tf.expand_dims(inputs, axis = 0)
        yhat = contraction(inputs, self.layers, self.n_nodes)
        if self.normalize:
            yhat, _ = tf.linalg.normalize(yhat, axis = -1)
            yhat = tf.where(
                tf.math.is_nan(yhat), tf.convert_to_tensor(0.5, dtype = yhat.dtype), yhat
            )
        return self.activation(yhat)


    def get_config(self):
        config = super().get_config()

        config.update(
            {
                "n_nodes"    : self.n_nodes, "hilbert_dim": self.hilbert_dim,
                "bond_dim"   : self.bond_dim, "output_dim": self.output_dim,
                "activation" : tf.keras.utils.serialize_keras_object(self.activation),
                "initializer": tf.keras.utils.serialize_keras_object(self.initializer),
            }
        )
        return config


def compute_4nodes(inputs, nodes):
    x = tf.unstack(inputs)
    first_layer = tf.einsum("i,j,ijkl->kl", x[1], x[2], nodes[0][0])
    second_layer = tf.einsum(
        "i,j,kl,ikm,jlo->mo", x[0], x[3], first_layer, nodes[1][0], nodes[1][1]
    )

    return tf.einsum("ij,ijk->k", second_layer, nodes[2][0])


def compute_6nodes(inputs, nodes):
    """
    First version of MERA contraction for 6 qubits (7 nodes in total)

    Parameters
    ----------
    inputs : tf.Tensor
        input tensor
    nodes : Sequence[tf.Tensor]
        trainable nodes

    Returns
    -------
    tf.Tensor
    """
    x = tf.unstack(inputs)
    first_layer = [tf.einsum("i,j,ijkl->kl", x[1], x[2], nodes[0][0]),
                   tf.einsum("i,j,ijkl->kl", x[3], x[4], nodes[0][1])]
    second_layer = tf.einsum(
        "i,j,kl,mn,iko,lma,jnb->bao", x[0], x[5], first_layer[0], first_layer[1], nodes[1][0],
        nodes[1][1], nodes[1][2],
    )

    return tf.einsum("bao,obx,axj", second_layer, nodes[2][0], nodes[3][0])


def compute_6nodes_v2(inputs: tf.Tensor, nodes: Sequence[tf.Tensor]) -> tf.Tensor:
    """
    Second version of MERA contraction for 6 qubits (8 nodes in total)

    Parameters
    ----------
    inputs : tf.Tensor
        input tensor
    nodes : Sequence[tf.Tensor]
        trainable nodes

    Returns
    -------
    tf.Tensor
    """
    first_layer, second_layer, third_layer, output_layer = nodes
    x = tf.unstack(inputs)

    post_first_layer = [tf.einsum("i,j,ijkl->kl", x[1], x[2], first_layer[0]),
                        tf.einsum("i,j,ijkl->kl", x[3], x[4], first_layer[1])]

    post_second_layer_pre = [
        tf.einsum("i,ija->ja", x[0], second_layer[0]),
        tf.einsum("n,nmd->md", x[-1], second_layer[-1])
    ]

    post_second_layer = tf.einsum(
        "jk,lm,ja,klbc,md->abcd", post_first_layer[0], post_first_layer[1],
        post_second_layer_pre[0], second_layer[1], post_second_layer_pre[1]
    )

    post_third_layer = tf.einsum(
        "abcd,abe,cdf->ef", post_second_layer, third_layer[0], third_layer[1]
    )

    return tf.einsum("ef,efg->g", post_third_layer, output_layer[0])


def compute_8nodes(inputs, nodes):
    x = tf.unstack(inputs)
    first_layer = [tf.einsum("i,j,ijkl->kl", x[1], x[2], nodes[0][0]),
                   tf.einsum("i,j,ijkl->kl", x[3], x[4], nodes[0][1]),
                   tf.einsum("i,j,ijkl->kl", x[5], x[6], nodes[0][2])]

    second_layer = tf.einsum(
        "i,jk,lm,no,p,ijr,kls,mnt,poz->rstz", x[0], first_layer[0], first_layer[1],
        first_layer[2], x[-1], nodes[1][0], nodes[1][1], nodes[1][2], nodes[1][3],
    )

    return tf.einsum(
        "rstz,sti,rzj,ijo->o", second_layer, nodes[2][0], nodes[3][0], nodes[4][0]
    )


def compute_16nodes(inputs: tf.Tensor, nodes: Sequence[tf.Tensor]) -> tf.Tensor:
    """Contract MERA with 16 qubit input"""
    x = tf.unstack(inputs)
    (first_mera_layer, first_layer, second_mera_layer, second_layer,
     third_mera_layer, third_layer, forth_layer) = nodes

    def mera_contraction(first: tf.Tensor, second: tf.Tensor, third: tf.Tensor):
        return tf.unstack(tf.einsum("i,j,ijkl->kl", first, second, third), axis = 0)

    output1 = [x[0]]
    for idx in range(7):
        tmp = mera_contraction(x[(idx * 2) + 1], x[(idx * 2) + 2], first_mera_layer[idx])
        output1.append(tmp[0])
        output1.append(tmp[1])
    output1.append(x[-1])

    output2 = [
        tf.einsum("i,j,ijk->k", output1[idx * 2], output1[(idx * 2) + 1], first_layer[idx])
        for idx in range(8)
    ]

    output3 = [output2[0]]
    for idx in range(3):
        tmp = mera_contraction(
            output2[(idx * 2) + 1], output2[(idx * 2) + 2], second_mera_layer[idx]
        )
        output3.append(tmp[0])
        output3.append(tmp[1])
    output3.append(output2[-1])

    output4 = [
        tf.einsum("i,j,ijk->k", output3[idx * 2], output3[(idx * 2) + 1], second_layer[idx])
        for idx in range(4)
    ]

    tmp = mera_contraction(output4[1], output4[2], third_mera_layer[0])
    output5 = [output4[0], tmp[0], tmp[1], output4[-1]]

    output6 = [
        tf.einsum("i,j,ijk->k", output5[idx * 2], output5[(idx * 2) + 1], third_layer[idx])
        for idx in range(2)
    ]

    return tf.einsum("i,j,ijk->k", output6[0], output6[1], forth_layer[0])


@tf.function
def contraction(x: tf.Tensor, nodes: Sequence[tf.Tensor], nnodes: int) -> tf.Tensor:
    if nnodes == 4:
        return tf.vectorized_map(lambda vec: compute_4nodes(vec, nodes), x)
    elif nnodes == 6:
        return tf.vectorized_map(lambda vec: compute_6nodes_v2(vec, nodes), x)
    elif nnodes == 8:
        return tf.vectorized_map(lambda vec: compute_8nodes(vec, nodes), x)
    elif nnodes == 16:
        return tf.vectorized_map(lambda vec: compute_16nodes(vec, nodes), x)
