from typing import Text, Sequence, Union, Optional, Dict, Callable, List

import tensorflow as tf


# TODO: Build a generic TTN layer

@tf.keras.utils.register_keras_serializable(package = 'tnqcircuits')
class TTN(tf.keras.layers.Layer):
    """
    Tensor Tree layer. Note that this class is not a generic implementation of TTNs. It is
    designed to execute specific constructions for given number of nodes.

    Parameters
    ----------
    n_nodes : int
        number of qubits
    hilbert_dim : int
        Hilbert space dimensions
    bond_dim : int
        bond dimensions
    output_dim : int
        output dimensions
    execute_as: Union[Text, Dict]
        This option tells the code to run as a "layer" within hybrid model execution or as a
        "model" to construct and run a complete tree. Default "layer"
        if given as dictionary, it expects the keys to be mode and nqubits
            mode: Text
                This option tells the code to run as a "layer" within hybrid model
                execution or as a "model" to construct and run a complete tree. default layer
            nqubits: int
                number of qubits. Only 4 or 6 are available. default 4
        (only available for n_nodes = 36)
    activation : Text
        activation function
    initializer : Text
        initializer for nodes
    kernel_regularizer: Text
        regularizer for trainable tensors
    kwargs :
        other keyword arguments for a keras layer
    """


    def __init__(
        self, n_nodes: Union[Sequence[int], int], hilbert_dim: int, bond_dim: int,
        output_dim: int, execute_as: Union[Text, Dict] = "layer", activation: Text = "square",
        initializer: Text = "glorot_uniform", kernel_regularizer: Optional[Text] = None,
        **kwargs,
    ):
        super(TTN, self).__init__(**kwargs)
        assert n_nodes in [4, 6, 8, 16, 36], f"Unknown number of nodes {n_nodes}"
        self.n_nodes = n_nodes
        self.hilbert_dim = hilbert_dim
        self.bond_dim = bond_dim
        self.output_dim = output_dim
        if isinstance(execute_as, str):
            assert execute_as in ["layer", "model"], f"Unknown execution command: {execute_as}"
            self.execute_as = {"mode": execute_as, "nqubits" : 4}
        elif isinstance(execute_as, dict):
            self.execute_as = {"mode": execute_as.get("mode", "layer"),
                               "nqubits" : int(execute_as.get("nqubits", 4))}
        self.activation: Callable[[tf.Tensor], tf.Tensor] = (
            tf.keras.activations.get(activation) if activation != "square" else tf.square
        )

        self.initializer = tf.initializers.get(initializer)
        self.kernel_regularizer = tf.keras.regularizers.get(kernel_regularizer)

        self.normalize = True if activation == "square" else False


    def build(self, input_shape: Sequence[int]) -> None:
        super().build(input_shape)

        self.layers = []
        if self.n_nodes == 4:
            self.layers.append(
                [
                    self.add_weight(
                    shape = (self.hilbert_dim, self.hilbert_dim, self.bond_dim),
                    name = f"layer_0_{inode}", trainable = True,
                    initializer = self.initializer, dtype = self.dtype,
                    regularizer = self.kernel_regularizer, ) for inode in range(2)
                ]
            )
            self.layers.append(
                [
                    self.add_weight(
                        shape = (self.bond_dim, self.bond_dim, self.output_dim),
                        name = f"output", trainable = True,
                        initializer = self.initializer, dtype = self.dtype,
                        regularizer = self.kernel_regularizer, )
                ]
            )
        elif self.n_nodes == 6:
            self.layers.append(
                [
                    self.add_weight(
                        shape = (self.hilbert_dim, self.hilbert_dim, self.bond_dim),
                        name = f"layer_0_{inode}", trainable = True,
                        initializer = self.initializer, dtype = self.dtype,
                        regularizer = self.kernel_regularizer, ) for inode in range(3)
                ]
            )
            self.layers.append(
                [
                    self.add_weight(
                        shape = (self.bond_dim, self.bond_dim, self.bond_dim),
                        name = "layer_1_0", trainable = True,
                        initializer = self.initializer, dtype = self.dtype,
                        regularizer = self.kernel_regularizer, )
                ]
            )
            self.layers.append(
                [
                    self.add_weight(
                        shape = (self.bond_dim, self.bond_dim, self.output_dim),
                        name = "output", trainable = True,
                        initializer = self.initializer, dtype = self.dtype,
                        regularizer = self.kernel_regularizer, )
                ]
            )
        elif self.n_nodes == 8:
            self.layers.append(
                [
                    self.add_weight(
                        shape = (self.hilbert_dim, self.hilbert_dim, self.bond_dim),
                        name = f"layer_0_{inode}", trainable = True,
                        initializer = self.initializer, dtype = self.dtype,
                        regularizer = self.kernel_regularizer, ) for inode in range(4)
                ]
            )
            self.layers.append(
                [
                    self.add_weight(
                        shape = (self.bond_dim, self.bond_dim, self.bond_dim),
                        name = f"layer_1_{inode}", trainable = True,
                        initializer = self.initializer, dtype = self.dtype,
                        regularizer = self.kernel_regularizer, ) for inode in range(2)
                ]
            )
            self.layers.append(
                [
                    self.add_weight(
                        shape = (self.bond_dim, self.bond_dim, self.output_dim),
                        name = f"output", trainable = True,
                        initializer = self.initializer, dtype = self.dtype,
                        regularizer = self.kernel_regularizer, )
                ]
            )

        elif self.n_nodes == 16:
            first_layer = [
                self.add_weight(shape = (self.hilbert_dim, self.hilbert_dim, self.bond_dim),
                                name = f"first_layer_{idx}", trainable = True,
                                initializer = self.initializer, dtype = self.dtype,
                                regularizer = self.kernel_regularizer)
                for idx in range(8)
            ]

            second_layer = [
                self.add_weight(
                    shape = (self.bond_dim, self.bond_dim, self.bond_dim),
                    name = f"second_layer_{idx}", trainable = True,
                    initializer = self.initializer, dtype = self.dtype,
                    regularizer = self.kernel_regularizer
                    )
                for idx in range(4)
            ]

            third_layer = [
                self.add_weight(
                        shape = (self.bond_dim, self.bond_dim, self.bond_dim),
                        name = f"third_layer_{idx}", trainable = True,
                        initializer = self.initializer, dtype = self.dtype,
                        regularizer = self.kernel_regularizer
                )
                for idx in range(2)
            ]

            forth_layer = [
                self.add_weight(
                        shape = (self.bond_dim, self.bond_dim, self.output_dim),
                        name = f"forth_layer_0", trainable = True,
                        initializer = self.initializer, dtype = self.dtype,
                        regularizer = self.kernel_regularizer
                )
            ]

            self.layers = [first_layer, second_layer, third_layer, forth_layer]

        elif self.n_nodes == 36:
            if self.execute_as["mode"] == "layer":
                if self.execute_as["nqubits"] == 4:
                    first_layer = []
                    for nrow in range(3):
                        first_layer.append([])
                        for ncol in range(3):
                            first_layer[-1].append(
                                self.add_weight(
                                    shape = (self.hilbert_dim, self.hilbert_dim, self.hilbert_dim,
                                             self.hilbert_dim, self.bond_dim),
                                    name = f"nrow_{nrow}_ncol_{ncol}_layer_0", trainable = True,
                                    initializer = self.initializer, dtype = self.dtype,
                                    regularizer = self.kernel_regularizer,
                                )
                            )

                    second_layer = [
                        self.add_weight(
                            shape = (self.bond_dim, self.bond_dim, 1),
                            name = f"idx_{idx}_layer_2", trainable = True,
                            initializer = self.initializer, dtype = self.dtype,
                            regularizer = self.kernel_regularizer,
                        )
                        for idx in range(3)
                    ] + [
                        self.add_weight(
                            shape = (self.bond_dim, self.bond_dim, self.bond_dim),
                            name = "idx_3_layer_2", trainable = True,
                            initializer = self.initializer, dtype = self.dtype,
                            regularizer = self.kernel_regularizer,
                        )
                    ] + [
                        self.add_weight(
                            shape = (self.bond_dim, self.bond_dim, 1),
                            name = "idx_4_layer_2", trainable = True,
                            initializer = self.initializer, dtype = self.dtype,
                            regularizer = self.kernel_regularizer,
                        )
                    ]
                elif self.execute_as["nqubits"] == 6:
                    first_layer = []
                    for nrow in range(3):
                        first_layer.append([])
                        for ncol in range(3):
                            if (nrow, ncol) in [(0,2), (1,2), (2,2), (1,0), (2,0)]:
                                shape = (self.hilbert_dim, self.hilbert_dim, self.hilbert_dim,
                                         self.hilbert_dim, self.bond_dim)
                            else:
                                shape = (self.hilbert_dim, self.hilbert_dim, self.hilbert_dim,
                                         self.hilbert_dim, 1)
                            first_layer[-1].append(
                                self.add_weight(
                                    shape = shape,
                                    name = f"nrow_{nrow}_ncol_{ncol}_layer_0", trainable = True,
                                    initializer = self.initializer, dtype = self.dtype,
                                    regularizer = self.kernel_regularizer,
                                )
                            )

                    second_layer = [
                        self.add_weight(
                            shape = (self.bond_dim, self.bond_dim, self.bond_dim),
                            name = f"idx_0_layer_2", trainable = True,
                            initializer = self.initializer, dtype = self.dtype,
                            regularizer = self.kernel_regularizer,
                        ),
                        self.add_weight(
                            shape = (self.bond_dim, self.bond_dim, 1),
                            name = f"idx_1_layer_2", trainable = True,
                            initializer = self.initializer, dtype = self.dtype,
                            regularizer = self.kernel_regularizer,
                        ),
                        self.add_weight(
                            shape = (self.bond_dim, self.bond_dim, 1),
                            name = f"idx_2_layer_2", trainable = True,
                            initializer = self.initializer, dtype = self.dtype,
                            regularizer = self.kernel_regularizer,
                        ),
                    ]
                else:
                    raise NotImplementedError(f"Nqubits = {self.execute_as['nqubits']} "
                                              f"has not been implemented.")

                self.layers = [first_layer, second_layer]

            else:
                first_layer = []
                for nrow in range(3):
                    first_layer.append([])
                    for ncol in range(3):
                        first_layer[-1].append(
                            self.add_weight(
                                shape = (self.hilbert_dim, self.hilbert_dim, self.hilbert_dim,
                                         self.hilbert_dim, self.bond_dim),
                                name = f"nrow_{nrow}_ncol_{ncol}_layer_0", trainable = True,
                                initializer = self.initializer, dtype = self.dtype,
                                regularizer = self.kernel_regularizer,
                            )
                        )

                second_layer = [
                    self.add_weight(
                        shape = (self.bond_dim, self.bond_dim, self.bond_dim),
                        name = f"idx_{idx}_layer_2", trainable = True,
                        initializer = self.initializer, dtype = self.dtype,
                        regularizer = self.kernel_regularizer,
                    ) for idx in range(5)
                ]

                third_layer = [
                    self.add_weight(
                        shape = (self.bond_dim, self.bond_dim, self.bond_dim),
                        name = f"idx_{idx}_layer_3", trainable = True,
                        initializer = self.initializer, dtype = self.dtype,
                        regularizer = self.kernel_regularizer,
                    ) for idx in range(2)
                ] + [
                    self.add_weight(
                        shape = (self.bond_dim, self.bond_dim, self.output_dim),
                        name = f"idx_2_layer_3", trainable = True,
                        initializer = self.initializer, dtype = self.dtype,
                        regularizer = self.kernel_regularizer,
                    )
                ]

                self.layers = [first_layer, second_layer, third_layer]

        else:
            raise NotImplementedError


    def call(self, inputs: tf.Tensor):
        if len(tf.shape(inputs)) == 1:
            inputs = tf.expand_dims(inputs, axis = 0)

        execute_as = -1
        if self.execute_as["mode"] == "model":
            execute_as = 1
        elif self.execute_as["mode"] == "layer" and  self.execute_as["nqubits"] == 4:
            execute_as = 0
        elif self.execute_as["mode"] == "layer" and  self.execute_as["nqubits"] == 6:
            execute_as = 2

        yhat = contraction(inputs, self.layers, self.n_nodes, execute_as = execute_as)
        if self.normalize:
            yhat, _ = tf.linalg.normalize(yhat, axis = -1)
            yhat = tf.where(
                tf.math.is_nan(yhat), tf.convert_to_tensor(0.5, dtype = yhat.dtype), yhat
            )
        return self.activation(yhat)


    def get_config(self):
        config = super().get_config()

        config.update(
            {
                "n_nodes"    : self.n_nodes, "hilbert_dim": self.hilbert_dim,
                "bond_dim"   : self.bond_dim, "output_dim": self.output_dim,
                "activation" : tf.keras.utils.serialize_keras_object(self.activation),
                "initializer": tf.keras.utils.serialize_keras_object(self.initializer),
            }
        )
        return config


def compute_4nodes(inputs, nodes):
    first_layer, second_layer = nodes
    x = tf.unstack(inputs)
    first_layer_out = [
        tf.einsum("i,j,ijk->k", x[0], x[1], first_layer[0]),
        tf.einsum("i,j,ijk->k", x[2], x[3], first_layer[1]),
    ]
    return tf.einsum("i,j,ijk->k", first_layer_out[0], first_layer_out[1], second_layer[0])


def compute_6nodes(inputs, nodes):
    first_layer, second_layer, third_layer = nodes
    x = tf.unstack(inputs)
    first_layer_out = [
        tf.einsum("i,j,ijk->k", x[0], x[1], first_layer[0]),
        tf.einsum("i,j,ijk->k", x[2], x[3], first_layer[1]),
        tf.einsum("i,j,ijk->k", x[4], x[5], first_layer[2]),
    ]
    second_layer_out = tf.einsum(
        "i,j,ijk->k", first_layer_out[0], first_layer_out[1], second_layer[0]
    )
    return tf.einsum("i,j,ijk->k", first_layer_out[-1], second_layer_out, third_layer[0])


def compute_8nodes(inputs, nodes):
    first_layer, second_layer, third_layer = nodes
    x = tf.unstack(inputs)
    first_layer_out = [
        tf.einsum("i,j,ijk->k", x[0], x[1], first_layer[0]),
        tf.einsum("i,j,ijk->k", x[2], x[3], first_layer[1]),
        tf.einsum("i,j,ijk->k", x[4], x[5], first_layer[2]),
        tf.einsum("i,j,ijk->k", x[6], x[7], first_layer[3]),
    ]
    second_layer_out = [
        tf.einsum("i,j,ijk->k", first_layer_out[0], first_layer_out[1], second_layer[0]),
        tf.einsum("i,j,ijk->k", first_layer_out[2], first_layer_out[3], second_layer[1]),

    ]
    return tf.einsum("i,j,ijk->k", second_layer_out[0], second_layer_out[1], third_layer[0])


def compute_16nodes(inputs: tf.Tensor, nodes: Sequence[tf.Tensor]) -> tf.Tensor:
    """Contract TTN with 16 nodes"""
    x = tf.unstack(inputs)
    first_layer, second_layer, third_layer, forth_layer = nodes

    output1 = [
        tf.einsum("i,j,ijk->k", x[idx * 2], x[(idx * 2) + 1], first_layer[idx]) for idx in range(8)
    ]

    output2 = [
        tf.einsum("i,j,ijk->k", output1[idx * 2], output1[(idx * 2) + 1], second_layer[idx])
        for idx in range(4)
    ]

    output3 = [
        tf.einsum("i,j,ijk->k", output2[idx * 2], output2[(idx * 2) + 1], third_layer[idx])
        for idx in range(2)
    ]

    return tf.einsum("i,j,ijk->k", output3[0], output3[1], forth_layer[0])

def compute_6x6nodes(inputs, nodes):
    first_layer, second_layer, third_layer = nodes
    x = [tf.unstack(ins, axis = 0) for ins in tf.unstack(inputs, axis = 0)]
    post_first_layer = [
        [tf.einsum("i,j,k,l,ijklm", x[0][0], x[0][1], x[1][0], x[1][1], first_layer[0][0]),
         tf.einsum("i,j,k,l,ijklm", x[0][2], x[0][3], x[1][2], x[1][3], first_layer[0][1]),
         tf.einsum("i,j,k,l,ijklm", x[0][4], x[0][5], x[1][4], x[1][5], first_layer[0][2])],
        [tf.einsum("i,j,k,l,ijklm", x[2][0], x[2][1], x[3][0], x[3][1], first_layer[1][0]),
         tf.einsum("i,j,k,l,ijklm", x[2][2], x[2][3], x[3][2], x[3][3], first_layer[1][1]),
         tf.einsum("i,j,k,l,ijklm", x[2][4], x[2][5], x[3][4], x[3][5], first_layer[1][2])],
        [tf.einsum("i,j,k,l,ijklm", x[4][0], x[4][1], x[5][0], x[5][1], first_layer[2][0]),
         tf.einsum("i,j,k,l,ijklm", x[4][2], x[4][3], x[5][2], x[5][3], first_layer[2][1]),
         tf.einsum("i,j,k,l,ijklm", x[4][4], x[4][5], x[5][4], x[5][5], first_layer[2][2])],
    ]
    post_second_layer = [
        tf.einsum("i,j,ijk", post_first_layer[0][0], post_first_layer[1][0], second_layer[0]),
        tf.einsum("i,j,ijk", post_first_layer[0][1], post_first_layer[1][1], second_layer[1]),
        tf.einsum("i,j,ijk", post_first_layer[0][2], post_first_layer[1][2], second_layer[2]),
        tf.einsum(
            "i,j,ijk", tf.einsum(
                "i,j,ijk", post_first_layer[2][0], post_first_layer[2][1], second_layer[3]
            ), post_first_layer[2][2], second_layer[4],
        )
    ]

    return tf.einsum("i,j,k,l,ijklm", *post_second_layer, third_layer)


def compute_6x6nodes_nq4(inputs, nodes):
    first_layer, second_layer = nodes
    x = [tf.unstack(ins, axis = 0) for ins in tf.unstack(inputs, axis = 0)]
    post_first_layer = [
        [tf.einsum("i,j,k,l,ijklm", x[0][0], x[0][1], x[1][0], x[1][1], first_layer[0][0]),
         tf.einsum("i,j,k,l,ijklm", x[0][2], x[0][3], x[1][2], x[1][3], first_layer[0][1]),
         tf.einsum("i,j,k,l,ijklm", x[0][4], x[0][5], x[1][4], x[1][5], first_layer[0][2])],
        [tf.einsum("i,j,k,l,ijklm", x[2][0], x[2][1], x[3][0], x[3][1], first_layer[1][0]),
         tf.einsum("i,j,k,l,ijklm", x[2][2], x[2][3], x[3][2], x[3][3], first_layer[1][1]),
         tf.einsum("i,j,k,l,ijklm", x[2][4], x[2][5], x[3][4], x[3][5], first_layer[1][2])],
        [tf.einsum("i,j,k,l,ijklm", x[4][0], x[4][1], x[5][0], x[5][1], first_layer[2][0]),
         tf.einsum("i,j,k,l,ijklm", x[4][2], x[4][3], x[5][2], x[5][3], first_layer[2][1]),
         tf.einsum("i,j,k,l,ijklm", x[4][4], x[4][5], x[5][4], x[5][5], first_layer[2][2])],
    ]
    post_second_layer = [
        tf.einsum("i,j,ijk", post_first_layer[0][0], post_first_layer[1][0], second_layer[0]),
        tf.einsum("i,j,ijk", post_first_layer[0][1], post_first_layer[1][1], second_layer[1]),
        tf.einsum("i,j,ijk", post_first_layer[0][2], post_first_layer[1][2], second_layer[2]),
        tf.einsum(
            "i,j,ijk", tf.einsum(
                "i,j,ijk", post_first_layer[2][0], post_first_layer[2][1], second_layer[3]
            ), post_first_layer[2][2], second_layer[4],
        )
    ]

    return tf.concat(post_second_layer, 0)


def compute_6x6nodes_nq6(inputs, nodes):
    first_layer, second_layer = nodes
    x = [tf.unstack(ins, axis = 0) for ins in tf.unstack(inputs, axis = 0)]
    post_first_layer = [
        [tf.einsum("i,j,k,l,ijklm", x[0][0], x[0][1], x[1][0], x[1][1], first_layer[0][0]),
         tf.einsum("i,j,k,l,ijklm", x[0][2], x[0][3], x[1][2], x[1][3], first_layer[0][1]),
         tf.einsum("i,j,k,l,ijklm", x[0][4], x[0][5], x[1][4], x[1][5], first_layer[0][2])],
        [tf.einsum("i,j,k,l,ijklm", x[2][0], x[2][1], x[3][0], x[3][1], first_layer[1][0]),
         tf.einsum("i,j,k,l,ijklm", x[2][2], x[2][3], x[3][2], x[3][3], first_layer[1][1]),
         tf.einsum("i,j,k,l,ijklm", x[2][4], x[2][5], x[3][4], x[3][5], first_layer[1][2])],
        [tf.einsum("i,j,k,l,ijklm", x[4][0], x[4][1], x[5][0], x[5][1], first_layer[2][0]),
         tf.einsum("i,j,k,l,ijklm", x[4][2], x[4][3], x[5][2], x[5][3], first_layer[2][1]),
         tf.einsum("i,j,k,l,ijklm", x[4][4], x[4][5], x[5][4], x[5][5], first_layer[2][2])],
    ]

    first_post_sec_layer = tf.einsum(
        "i,j,ijk",
        tf.einsum("i,j,ijk", post_first_layer[0][2], post_first_layer[1][2], second_layer[0]),
        post_first_layer[2][2], second_layer[1],
    )

    post_second_layer = [
        first_post_sec_layer,
        tf.einsum("i,j,ijk", post_first_layer[1][0], post_first_layer[2][0], second_layer[2]),
    ]

    return tf.concat([
        post_first_layer[0][0], post_first_layer[0][1], post_first_layer[1][1],
        post_first_layer[2][1], post_second_layer[1], post_second_layer[0],
    ], 0)


def compute_6x6nodes_model(inputs, nodes):
    first_layer, second_layer, third_layer = nodes
    x = [tf.unstack(ins, axis = 0) for ins in tf.unstack(inputs, axis = 0)]
    post_first_layer = [
        [tf.einsum("i,j,k,l,ijklm", x[0][0], x[0][1], x[1][0], x[1][1], first_layer[0][0]),
         tf.einsum("i,j,k,l,ijklm", x[0][2], x[0][3], x[1][2], x[1][3], first_layer[0][1]),
         tf.einsum("i,j,k,l,ijklm", x[0][4], x[0][5], x[1][4], x[1][5], first_layer[0][2])],
        [tf.einsum("i,j,k,l,ijklm", x[2][0], x[2][1], x[3][0], x[3][1], first_layer[1][0]),
         tf.einsum("i,j,k,l,ijklm", x[2][2], x[2][3], x[3][2], x[3][3], first_layer[1][1]),
         tf.einsum("i,j,k,l,ijklm", x[2][4], x[2][5], x[3][4], x[3][5], first_layer[1][2])],
        [tf.einsum("i,j,k,l,ijklm", x[4][0], x[4][1], x[5][0], x[5][1], first_layer[2][0]),
         tf.einsum("i,j,k,l,ijklm", x[4][2], x[4][3], x[5][2], x[5][3], first_layer[2][1]),
         tf.einsum("i,j,k,l,ijklm", x[4][4], x[4][5], x[5][4], x[5][5], first_layer[2][2])],
    ]
    post_second_layer = [
        tf.einsum("i,j,ijk", post_first_layer[0][0], post_first_layer[1][0], second_layer[0]),
        tf.einsum("i,j,ijk", post_first_layer[0][1], post_first_layer[1][1], second_layer[1]),
        tf.einsum("i,j,ijk", post_first_layer[0][2], post_first_layer[1][2], second_layer[2]),
        tf.einsum(
            "i,j,ijk", tf.einsum(
                "i,j,ijk", post_first_layer[2][0], post_first_layer[2][1], second_layer[3]
            ), post_first_layer[2][2], second_layer[4],
        )
    ]

    post_third_layer_1 = tf.einsum(
        "i,j,ijk", post_second_layer[0], post_second_layer[1], third_layer[0]
    )
    post_third_layer_2 = tf.einsum(
        "i,j,ijk", post_third_layer_1, post_second_layer[2], third_layer[1]
    )

    return tf.einsum("i,j,ijk", post_third_layer_2, post_second_layer[-1], third_layer[2])


@tf.function
def contraction(
        x: tf.Tensor, nodes: Sequence[tf.Tensor], nnodes: int, execute_as: int = 0
) -> tf.Tensor:
    if nnodes == 4:
        return tf.vectorized_map(lambda vec: compute_4nodes(vec, nodes), x)
    elif nnodes == 6:
        return tf.vectorized_map(lambda vec: compute_6nodes(vec, nodes), x)
    elif nnodes == 8:
        return tf.vectorized_map(lambda vec: compute_8nodes(vec, nodes), x)
    elif nnodes == 16:
        return tf.vectorized_map(lambda vec: compute_16nodes(vec, nodes), x)
    elif nnodes == 36:
        if execute_as == 0:
            return tf.vectorized_map(lambda vec: compute_6x6nodes_nq4(vec, nodes), x)
        elif execute_as == 1:
            return tf.vectorized_map(lambda vec: compute_6x6nodes_model(vec, nodes), x)
        elif execute_as == 2:
            return tf.vectorized_map(lambda vec: compute_6x6nodes_nq6(vec, nodes), x)
