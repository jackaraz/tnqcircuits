import inspect
import os
from collections.abc import Iterable

import pennylane as qml
import tensorflow as tf

tf.keras.backend.set_floatx('float64')
os.environ['TF_CPP_MIN_LOG_LEVEL'] = '3'

@tf.keras.utils.register_keras_serializable(package='tnqcircuits')
class QuantumLayer(tf.keras.layers.Layer):
    """
    based on pennylane/qnn/keras.py
    """

    _input_arg = "inputs"


    @staticmethod
    def set_input_name(name):
        QuantumLayer._input_arg = name


    def __init__(self, qnode, weight_shapes: dict, output_dim, weight_specs: dict, **kwargs):
        super(QuantumLayer, self).__init__(dynamic = True, **kwargs)

        self._signature_validation(qnode, weight_shapes)

        if qnode.diff_method == "backprop" and qnode.device.short_name == "default.qubit.tf":
            if qnode.device.shots is not None:
                qnode.device.shots = None
            try:
                ws = {k: tf.random.uniform(i, dtype = self.dtype) for k, i in
                      weight_shapes.items()}
                vectorize(qnode)(
                    tf.random.uniform((10, qnode.device.num_wires), dtype = self.dtype), **ws
                )
                self.batched_qnode = vectorize(qnode)
            except Exception as err:
                print("WARNING: Vectorization of the quantum circuit has failed, reducing to "
                      "parallelized map which might be slower than the vectorized version.")
                ws = {k: tf.random.uniform(i, dtype = self.dtype) for k, i in
                      weight_shapes.items()}
                map_fn(qnode)(
                    tf.random.uniform((10, qnode.device.num_wires), dtype = self.dtype), **ws
                )
                self.batched_qnode = map_fn(qnode)
        else:
            self.batched_qnode = batch_input_tf(qnode)

        self.mapped_qnode = map_fn(qnode)

        self.weight_shapes = {weight: (
            tuple(size) if isinstance(size, Iterable) else (size,) if size > 1 else ()) for
            weight, size in weight_shapes.items()}

        if isinstance(output_dim, Iterable) and len(output_dim) > 1:
            self.output_dim = tuple(output_dim)
        else:
            self.output_dim = output_dim[0] if isinstance(output_dim, Iterable) else output_dim

        self.weight_specs = weight_specs if weight_specs is not None else {}

        self.qnode_weights = {}


    def _signature_validation(self, qnode, weight_shapes):
        sig = inspect.signature(qnode.func).parameters

        if QuantumLayer._input_arg not in sig:
            raise TypeError(
                f"QNode must include an argument with name {QuantumLayer._input_arg} "
                f"for inputting data"
            )

        if QuantumLayer._input_arg in set(weight_shapes.keys()):
            raise ValueError(
                f"{QuantumLayer._input_arg} argument should not have its dimension specified in "
                f"weight_shapes"
            )

        param_kinds = [p.kind for p in sig.values()]

        if inspect.Parameter.VAR_POSITIONAL in param_kinds:
            raise TypeError("Cannot have a variable number of positional arguments")

        if inspect.Parameter.VAR_KEYWORD not in param_kinds:
            if set(weight_shapes.keys()) | {QuantumLayer._input_arg} != set(sig.keys()):
                raise ValueError(
                    "Must specify a shape for every non-input parameter in the QNode"
                )


    def build(self, input_shape):
        super().build(input_shape)

        for weight, size in self.weight_shapes.items():
            spec = self.weight_specs.get(weight, {})
            self.qnode_weights[weight] = self.add_weight(name = weight, shape = size, **spec)


    def get_config(self):
        config = super().get_config()

        config.update(
            {"output_dim"   : self.output_dim, "weight_specs": self.weight_specs,
             "weight_shapes": self.weight_shapes}
        )
        return config


    def compute_output_shape(self, input_shape):
        return tf.TensorShape([input_shape[0]]).concatenate(self.output_dim)


    def call(self, inputs):
        if len(tf.shape(inputs)) == 1:
            inputs = tf.expand_dims(inputs, 0)

        kwargs = {**{QuantumLayer._input_arg: inputs},
                  **{k: 1.0 * w for k, w in self.qnode_weights.items()}}

        return self.batched_qnode(**kwargs)


    def map(self, inputs: tf.Tensor):
        if len(tf.shape(inputs)) == 1:
            inputs = tf.expand_dims(inputs, 0)

        kwargs = {**{QuantumLayer._input_arg: inputs},
                  **{k: 1.0 * w for k, w in self.qnode_weights.items()}}

        return self.mapped_qnode(**kwargs)


@qml.batch_transform
def batch_input_tf(tape: qml.tape.QuantumTape):
    # https://github.com/PennyLaneAI/pennylane/issues/2037
    parameters = tape.get_parameters(trainable_only = False)

    unstacked_inpt = tf.unstack(parameters[0], axis = 0)
    output = [[x] + parameters[1:] for x in unstacked_inpt]

    # Construct new output tape with unstacked inputs
    output_tapes = []
    for params in output:
        new_tape = tape.copy(copy_operations = True)
        new_tape.set_parameters(params, trainable_only = False)
        output_tapes.append(new_tape)

    return output_tapes, lambda x: qml.math.squeeze(qml.math.stack(x))

def vectorize(circ):
    @tf.function
    def func(inputs, weights):
        return tf.vectorized_map(lambda vec: circ(vec, weights), inputs)

    return func

def map_fn(circ):
    @tf.function
    def func(inputs, weights):
        return tf.map_fn(
            lambda vec: circ(vec, weights), inputs, parallel_iterations = 1000,
            dtype = inputs.dtype, fn_output_signature = inputs.dtype,
        )

    return func